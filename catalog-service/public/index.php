<?php

//phpinfo();

require __DIR__ . '/../vendor/autoload.php';

// Configure and Create Slim app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

//Register Services
require __DIR__ . '/../src/services.php';

//Register Routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
