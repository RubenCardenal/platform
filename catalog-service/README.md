# Catalog Service

Proof of Concept of a Microservice to decouple Catalog Search and Product Pages from Magento

### Installing

Clone the repository, compose docker, run composer go!


First run

```
git clone <this repository>
docker-compose up --build

#Open a new terminal window for the location of the code and execute
docker-compose exec app composer install
```

Subsequent runs

```
docker-compose up
```

For updating PHP Dependencies
```
docker-compose exec app composer install
```
