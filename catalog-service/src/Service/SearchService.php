<?php

namespace Deporvillage\CatalogService\Service;

use Deporvillage\CatalogService\Exception\NotFoundException;

class SearchService {
    
    private $elasticClient;

    public function __construct($elasticClient)
    {
        $this->elasticClient = $elasticClient;
    }

    public function search($text, $facets)
    {
        $params = [
            'index' => 'bank',
            'type' => '_doc',
            'body' => [
                'query' => [
                    "bool" => [
                        "must" => [
                            'multi_match' => [
                                "query" => $text,
                                "fields" => ["firstname", "address"]
                            ],  
                        ],
                        // "filter" => [
                        //     "terms" => [
                        //         "age" => [23, 28]
                        //     ]
                        // ] //For adding faceted filtering
                    ]                    
                ]
            ]
        ];

        if (!empty($facets)) {
            $params['body']['query']['bool']['filter'] = array();
            foreach ($facets as $key => $value) {
                $params['body']['query']['bool']['filter']['terms'] = array();
                $params['body']['query']['bool']['filter']['terms'][$key] = $value; 
                //This doesn;t work for "keyword" type of data
            }
        } else {
            //Add aggregations when no facet is applied             
            $aggs = [
                "facet_state" => [
                    "terms" => [
                        "field" => "state.keyword"
                    ]
                ],
                "facet_gender" => [
                    "terms" => [
                        "field" => "gender.keyword"
                    ]
                ],
                "slider_age_min" => [
                    "min" => [
                        "field" => "age"
                    ]
                ],
                "slider_age_max" => [
                    "max" => [
                        "field" => "age"
                    ]
                ]
            ];
            $params["body"]["aggs"] = $aggs;
        }

        // var_dump($params);
        // die;

        $results = $this->elasticClient->search($params);
        $facets = $this->getFacets($results['aggregations']);

        $return = array();
        $return["result"] = $this->getResults($results);
        $return["facets"] = $facets;
        return $return;
    }

    private function getResults($elasticResults)
    {
        $hits = $elasticResults["hits"]["hits"];
        $results = array();
        foreach ($hits as $key => $hit) {
            $results[] = $hit["_source"];
        }
        
        return $results;
    }

    private function getFacets($aggregations)
    {
        $facets = array();
        foreach ($aggregations as $key => $value) {
            if (strpos($key, 'facet_') === 0) {
                $facet = array();
                $facet["values"] = array();
                foreach ($value['buckets'] as $bucketKey => $bucketValue) {
                    $facet["values"][] = $bucketValue["key"];
                }
                $facets[$key] = $facet["values"];
            }
        }

        return $facets;
    }

    private function getSliders($aggregations)
    {
        $sliders = array();
        foreach ($aggregations as $key => $value) {
            if (strpos($key, 'slider_') === 0) {


            }
        }

        return $sliders;
    }
    
}