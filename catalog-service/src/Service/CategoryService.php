<?php

namespace Deporvillage\CatalogService\Service;

use Deporvillage\CatalogService\Exception\NotFoundException;



class CategoryService {

    private $elasticClient;

    public function __construct($elasticClient)
    {
        $this->elasticClient = $elasticClient;
    }

    /**
     * @return
     * 
     */
    public function getCategory($id)
    {
        $params = [
            'index' => 'bank',
            'type' => '_doc',
            'body' => [
                'query' => [
                    'match' => [
                        'account_number' => $id
                    ]
                ]
            ]
        ];
        $results = $this->elasticClient->search($params);
        if($results['hits']['total'] == 0) {
            throw new NotFoundException();
        }
        return $results['hits']['hits'][0]['_source'];
    }


}