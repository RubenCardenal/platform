<?php

use Elasticsearch\ClientBuilder;
use Deporvillage\CatalogService\Service\SearchService;
use Deporvillage\CatalogService\Service\CategoryService;
// DIC configuration
$container = $app->getContainer();

$container['client.elastic'] = function($c) {
    $hosts = ['http://docker.for.mac.localhost:9200'];
    $clientBuilder = ClientBuilder::create();   // Instantiate a new ClientBuilder
    $clientBuilder->setHosts($hosts);           // Set the hosts
    $client = $clientBuilder->build();
    return $client;
};

//Category Service
$container['service.category'] = function ($c) {
    return new CategoryService($c->get('client.elastic'));
};

//Search Service
$container['service.search'] = function ($c) {
    return new SearchService($c->get('client.elastic'));
};


// // view renderer
// $container['renderer'] = function ($c) {
//     $settings = $c->get('settings')['renderer'];
//     return new Slim\Views\PhpRenderer($settings['template_path']);
// };
// // monolog
// $container['logger'] = function ($c) {
//     $settings = $c->get('settings')['logger'];
//     $logger = new Monolog\Logger($settings['name']);
//     $logger->pushProcessor(new Monolog\Processor\UidProcessor());
//     $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
//     return $logger;
// };