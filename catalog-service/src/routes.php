<?php

use Slim\Http\Request;
use Slim\Http\Response;
use Deporvillage\ShippingService\Model\Carrier;
use Deporvillage\CatalogService\Service\SearchService;
use Deporvillage\ShippingService\Model\CarrierProduct;
use Deporvillage\ShippingService\Model\Carrier;
use Deporvillage\CatalogService\Exception\NotFoundException;
use Deporvillage\ShippingService\Service\AvailabilityService;

// Define app routes
$app->get('/hello/{name}', function ($request, $response, $args) {
    return $response->write("Hello " . $args['name']);
});

$app->get('/', function($request, $response, $args){
   return $response->write("Yeah!"); 
});

$app->get("/category/{id}", function($request, $response, $args){
    
    /**
     * @var CategoryService
     */
    $categoryService = $this->get('service.category'); //$this is the container :)
    try {
        return $response->withJson($categoryService->getCategory($args['id']) , 200);
    }
    catch (NotFoundException $e) {
        return $response->withJson(null, 404);
    }

});

$app->get("/search", function($request, $response, $args) {
   
    $requestParams = $request->getParams();
    $queryText = $requestParams['q'] != null ? $requestParams['q'] : "";

    $facets = array();
    foreach ($requestParams as $key => $value)  {
        if (strpos($key, "facet_") === 0 && $value != '') {
            $facetKey = str_replace("facet_", "", $key);
            $facets[$facetKey] = explode(",", $value);
        }
    }

    /**
     * @var SearchService
     */
    $searchService = $this->get('service.search');
    try {
        return $response->withJson($searchService->search($queryText, $facets) , 200);
    }
    catch (NotFoundException $e) {
        return $response->withJson(null, 404);
    }
    
});
