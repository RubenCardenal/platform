<?php

/**
 * @SWG\Swagger(
 *     schemes={"https"},
 *     host=API_HOST,
 *     basePath="/"
 * )
 *
 * @SWG\Info(
 *     title="Deporvillage Mailing API",
 *     version="1.0.0"
 * )
 */

/**
 * @SWG\Tag(
 *   name="Auth",
 *   description="Authentication",
 *   @SWG\ExternalDocumentation(
 *     description="",
 *     url=""
 *   )
 * )
 *
 * @SWG\Tag(
 *     name="Message",
 *     description="Send a new message on the fly",
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 *
 * @SWG\Tag(
 *     name="Draft",
 *     description="Create a draft message, upload attachments and then send it",
 *     @SWG\ExternalDocumentation(
 *         description="",
 *         url=""
 *     )
 * )
 */

/**
 *
 * @SWG\Parameter(
 *     description="Token",
 *     in="header",
 *     name="X-Token",
 *     required=true,
 *     type="string"
 * )
 *
 * @SWG\Parameter(
 *     name="Accept-Language",
 *     description="Specify which shop to use",
 *     in="header",
 *     required=true,
 *     type="string",
 *     enum={"es-ES","fr-FR","it-IT","pt-PT"}
 * )
 *
 * @SWG\Parameter(
 *     name="attachment",
 *     description="Attachment to upload",
 *     in="formData",
 *     required=true,
 *     type="file"
 * )
 *
 * @SWG\Parameter(
 *     name="messageId",
 *     description="ID of the message",
 *     in="path",
 *     required=true,
 *     type="integer",
 *     format="int64"
 * )
 */

/**
 * @SWG\Response(
 *     response="200OK",
 *     description="OK",
 *     @SWG\Schema(
 *         title="Response",
 *         @SWG\Property(
 *             property="error",
 *             type="boolean",
 *             example=false
 *         ),
 *         @SWG\Property(
 *             property="code",
 *             type="int",
 *             example=200
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string",
 *             example="The request has succeeded"
 *         ),
 *         @SWG\Property(
 *             property="data",
 *             type="object"
 *         )
 *     )
 * )
 *
 * @SWG\Response(
 *     response="201Created",
 *     description="Created",
 *     @SWG\Schema(
 *         title="Response",
 *         @SWG\Property(
 *             property="error",
 *             type="boolean",
 *             example=false
 *         ),
 *         @SWG\Property(
 *             property="code",
 *             type="int",
 *             example=201
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string",
 *             example="Message created successfully"
 *         ),
 *         @SWG\Property(
 *             property="data",
 *             type="object",
 *             title="Message",
 *             @SWG\Property(
 *                 property="messageId",
 *                 description="Message ID",
 *                 type="integer",
 *                 example="1"
 *             ),
 *         )
 *     )
 * )
 *
 * @SWG\Response(
 *     response="400BadRequest",
 *     description="Bad Request",
 *     @SWG\Schema(
 *         title="Response",
 *         @SWG\Property(
 *             property="error",
 *             type="boolean",
 *             example=true
 *         ),
 *         @SWG\Property(
 *             property="code",
 *             type="int",
 *             example=400
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string",
 *             example="Bad Request"
 *         ),
 *         @SWG\Property(
 *             property="data",
 *             type="object"
 *         )
 *     )
 * )
 *
 * @SWG\Response(
 *     response="401Unauthorized",
 *     description="Unauthorized",
 *     @SWG\Schema(
 *         title="Response",
 *         @SWG\Property(
 *             property="error",
 *             type="boolean",
 *             example=true
 *         ),
 *         @SWG\Property(
 *             property="code",
 *             type="int",
 *             example=401
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string",
 *             example="Authentication failed"
 *         ),
 *         @SWG\Property(
 *             property="data",
 *             type="object"
 *         )
 *     )
 * )
 *
 * @SWG\Response(
 *     response="404NotFound",
 *     description="Not Found",
 *     @SWG\Schema(
 *         title="Response",
 *         @SWG\Property(
 *             property="error",
 *             type="boolean",
 *             example=true
 *         ),
 *         @SWG\Property(
 *             property="code",
 *             type="int",
 *             example=404
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string",
 *             example="Message not found"
 *         ),
 *         @SWG\Property(
 *             property="data",
 *             type="object"
 *         )
 *     )
 * )
 *
 * @SWG\Response(
 *     response="500InternalServerError",
 *     description="Internal Server Error",
 *     @SWG\Schema(
 *         title="Response",
 *         @SWG\Property(
 *             property="error",
 *             type="boolean",
 *             example=true
 *         ),
 *         @SWG\Property(
 *             property="code",
 *             type="int",
 *             example=500
 *         ),
 *         @SWG\Property(
 *             property="message",
 *             type="string",
 *             example="Internal Server Error"
 *         ),
 *         @SWG\Property(
 *             property="data",
 *             type="object"
 *         )
 *     )
 * )
 */