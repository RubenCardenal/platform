<?php

namespace Deporvillage\NotificationService\Service\Message;

use Deporvillage\NotificationService\Entity\Attachment\Attachment;
use Deporvillage\NotificationService\Entity\Message\Message as Message;
use Deporvillage\NotificationService\Service\Response;

interface MessageServiceInterface
{
    /**
     * @param Message $message
     * @return Response
     */
    public function send(Message $message);

    /**
     * @param Message $message
     * @return Response
     */
    public function save(Message $message);

    /**
     * @param int $messageId
     * @return Response
     */
    public function sendById($messageId);

    /**
     * @param int $messageId
     * @param Attachment $attachment
     * @return Response
     */
    public function uploadAttachment($messageId, Attachment $attachment);
}