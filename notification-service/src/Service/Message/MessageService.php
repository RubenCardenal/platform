<?php

namespace Deporvillage\NotificationService\Service\Message;

use Deporvillage\NotificationService\Entity\Attachment\Attachment;
use Deporvillage\NotificationService\Model\Message\MessageModelInterface;
use Deporvillage\NotificationService\Entity\Message\Message;
use Deporvillage\NotificationService\Service\Response;
use Deporvillage\NotificationService\Service\Mail\MailServiceInterface as MailService;
use Deporvillage\NotificationService\Service\Validator\ValidatorInterface as Validator;
use Slim\Views\Twig;
use Psr\Log\LoggerInterface as Logger;
use Teapot\StatusCode;

class MessageService implements MessageServiceInterface
{
    /** @var Validator */
    protected $validator;

    /** @var MessageModelInterface */
    protected $messageModel;

    /** @var MailService */
    protected $mailService;

    /** @var Twig */
    protected $view;

    /** @var Logger */
    protected $logger;

    /**
     * @param Validator $validator
     * @param MessageModelInterface $messageModel
     * @param MailService $mailService
     * @param Twig $view
     * @param Logger $logger
     */
    public function __construct(
        Validator $validator,
        MessageModelInterface $messageModel,
        MailService $mailService,
        Twig $view,
        Logger $logger
    ) {
        $this->validator = $validator;
        $this->messageModel = $messageModel;
        $this->mailService = $mailService;
        $this->view = $view;
        $this->logger = $logger;
    }

    /**
     * @param Message $message
     * @return Response
     */
    public function send(Message $message)
    {
        $this->validator->validate($message);
        $this->prepareMessage($message);

        try {
            $result = $this->mailService->send($message);
        } catch (\Exception $e) {
            return new Response(
                StatusCode::INTERNAL_SERVER_ERROR,
                'Message could not be sent: ' . get_class($e) . ' - ' . $e->getMessage()
            );
        }

        if ($result !== true) {
            return new Response(
                StatusCode::BAD_REQUEST, 'Message could not be sent', $result
            );
        }

        return new Response(
            StatusCode::OK, 'Message was sent successfully'
        );
    }

    /**
     * @param Message $message
     * @return Response
     */
    public function save(Message $message)
    {
        $this->validator->validate($message);
        $this->prepareMessage($message);

        try {
            $message = $this->messageModel->save($message);
        } catch (\Exception $e) {
            return new Response(
                StatusCode::INTERNAL_SERVER_ERROR,
                'Message could not be created: ' . get_class($e) . ' - ' . $e->getMessage()
            );
        }

        return new Response(
            StatusCode::CREATED, 'Message was created successfully', ['messageId' => $message->getId()]
        );
    }

    /**
     * @param int $messageId
     * @param Attachment $attachment
     * @return Response
     */
    public function uploadAttachment($messageId, Attachment $attachment)
    {
        try {
            if (!$this->messageModel->exists($messageId)) {
                return new Response(
                    StatusCode::NOT_FOUND, 'Message not found'
                );
            }
            $result = $this->messageModel->uploadAttachment($messageId, $attachment);
        } catch (\Exception $e) {
            return new Response(
                StatusCode::INTERNAL_SERVER_ERROR,
                'Upload attachment failed: ' . get_class($e) . ' - ' . $e->getMessage()
            );
        }

        if ($result === false) {
            return new Response(
                StatusCode::BAD_REQUEST, 'Attachment could not be uploaded'
            );
        }

        return new Response(
            StatusCode::OK, 'Attachment was uploaded successfully'
        );
    }

    /**
     * @param int $messageId
     * @return Response
     */
    public function sendById($messageId)
    {
        try {
            $message = $this->messageModel->findById($messageId);
            if (!$message) {
                return new Response(
                    StatusCode::NOT_FOUND, 'Message not found'
                );
            }
            $result = $this->mailService->send($message);
        } catch (\Exception $e) {
            return new Response(
                StatusCode::INTERNAL_SERVER_ERROR,
                'Message could not be sent: ' . get_class($e) . ' - ' . $e->getMessage()
            );
        }

        if ($result !== true) {
            return new Response(
                StatusCode::BAD_REQUEST, 'Message could not be sent', $result
            );
        }

        return new Response(
            StatusCode::OK, 'Message was sent successfully'
        );
    }

    /**
     * @param Message $message
     */
    private function prepareMessage(Message $message)
    {
        $subject = $this->view->fetchFromString($message->getSubject(), $message->getTemplateVars());
        $message->setSubject($subject);

        $content = $this->view->fetch($message->getTemplatePath(), $message->getTemplateVars());
        $message->setContent($content);
    }
}
