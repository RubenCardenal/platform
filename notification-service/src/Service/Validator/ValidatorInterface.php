<?php

namespace Deporvillage\NotificationService\Service\Validator;

use Deporvillage\NotificationService\Entity\Message\Message;

interface ValidatorInterface
{
    /**
     * @param Message $message
     * @return boolean
     */
    public function validate(Message $message);
}