<?php

namespace Deporvillage\NotificationService\Service\Validator;

use Deporvillage\NotificationService\Entity\Message\Message;
use Deporvillage\NotificationService\Entity\Mail\Mail;
use Teapot\StatusCode;

class ValidatorService implements ValidatorInterface
{
    protected $allowedLanguages = ['es-ES', 'fr-FR', 'it-IT', 'pt-PT'];

    public function validate(Message $message)
    {
        $this->validateLanguage($message);
        $this->validateFrom($message);
        $this->validateTo($message);
        $this->validateCc($message);
        $this->validateBcc($message);
        $this->validateReplyTo($message);
        $this->validateSubject($message);
        $this->validateTemplateVars($message);

        return true;
    }

    private function validateLanguage(Message $message)
    {
        if (!in_array($message->getLanguage(), $this->allowedLanguages)) {
            throw new \Exception(
                'Field Accept-Language must be one of ' . implode('|', $this->allowedLanguages),
                StatusCode::BAD_REQUEST
            );
        }
    }

    private function validateFrom(Message $message)
    {
        if ($message->isRequired('from') || !empty($message->getFrom())) {
            $this->validateMail('from', $message->getFrom());
        }
    }

    private function validateTo(Message $message)
    {
        if ($message->isRequired('to') || !empty($message->getTo())) {
            $this->validateMailList('to', $message->getTo());
        }
    }

    private function validateCc(Message $message)
    {
        if ($message->isRequired('cc') || !empty($message->getCc())) {
            $this->validateMailList('cc', $message->getCc());
        }
    }

    private function validateBcc(Message $message)
    {
        if ($message->isRequired('bcc') || !empty($message->getBcc())) {
            $this->validateMailList('bcc', $message->getBcc());
        }
    }

    private function validateReplyTo(Message $message)
    {
        if ($message->isRequired('replyTo') || !empty($message->getReplyTo())) {
            $this->validateMail('replyTo', $message->getReplyTo());
        }
    }

    private function validateMail($fieldName, $fieldValue)
    {
        if (!($fieldValue instanceof Mail)) {
            throw new \Exception('Field <' . $fieldName . '> is required', StatusCode::BAD_REQUEST);
        }
        if (!filter_var($fieldValue->getEmail(), FILTER_VALIDATE_EMAIL)) {
            throw new \Exception('Field <' . $fieldName . '> email is not valid', StatusCode::BAD_REQUEST);
        }
    }

    private function validateMailList($fieldName, $fieldValue)
    {
        if (!is_array($fieldValue) || count($fieldValue) === 0) {
            throw new \Exception('Field <' . $fieldName . '> must be an array', StatusCode::BAD_REQUEST);
        }

        foreach ($fieldValue as $value) {
            $this->validateMail($fieldName, $value);
        }
    }

    private function validateSubject(Message $message)
    {
        if ($message->isRequired('subject')) {
            if (empty($message->getSubject())) {
                throw new \Exception('Field <subject> is required', StatusCode::BAD_REQUEST);
            }
        }
    }

    private function validateTemplateVars(Message $message)
    {
        if ($message->isRequired('templateVars')) {
            if (empty($message->getTemplateVars())) {
                throw new \Exception('Field <templateVars> is required', StatusCode::BAD_REQUEST);
            }
        }
    }
}
