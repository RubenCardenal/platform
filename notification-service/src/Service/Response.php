<?php

namespace Deporvillage\NotificationService\Service;

class Response
{
    /** @var boolean */
    public $error;

    /** @var int */
    public $code;

    /** @var string */
    public $message;

    /** @var mixed|null */
    public $data;

    /**
     * @param int $code
     * @param string $message
     * @param mixed $data
     */
    public function __construct($code, $message, $data = null)
    {
        $this->error = ($code > 100 && $code < 400) ? false : true;
        $this->code = (int)$code;
        $this->message = $message;
        $this->data = (object)$data;
    }
}

