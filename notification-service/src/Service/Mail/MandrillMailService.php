<?php

namespace Deporvillage\NotificationService\Service\Mail;

use Deporvillage\NotificationService\Entity\Mail\Mail;
use Deporvillage\NotificationService\Entity\Message\Message;
use Mandrill;
use Teapot\StatusCode;

class MandrillMailService implements MailServiceInterface
{
    const MANDRILL_STATUS_SENT = 'sent';
    const MANDRILL_STATUS_QUEUED = 'queued';
    const MANDRILL_STATUS_REJECTED = 'rejected';

    protected $mandrill;
    protected $async = false;
    protected $ipPool = 'Main Pool';

    /**
     * @param Mandrill $mandrill
     */
    public function __construct(Mandrill $mandrill)
    {
        $this->mandrill = $mandrill;
    }

    /**
     * @param Message $message
     * @return array|bool
     */
    public function send(Message $message)
    {
        $data = $this->prepareData($message);
        $result = $this->mandrill->messages->send($data, $this->async, $this->ipPool);

        if ($result[0]['status'] === self::MANDRILL_STATUS_SENT ||
            $result[0]['status'] === self::MANDRILL_STATUS_QUEUED
        ) {
            return true;
        }

        return $result;
    }

    /**
     * @param Message $message
     * @return array
     */
    private function prepareData(Message $message)
    {
        $data = [
            'html' => $message->getContent(),
            'subject' => $message->getSubject(),
            'from_email' => $message->getFrom()->getEmail(),
            'from_name' => $message->getFrom()->getName(),
        ];

        $data['to'] = $this->prepareRecipientsData($message->getTo());

        if ($message->getCc()) {
            $data['to'] = array_merge($data['to'], $this->prepareRecipientsData($message->getCc(), 'cc'));
        }

        if ($message->getBcc()) {
            $data['to'] = array_merge($data['to'], $this->prepareRecipientsData($message->getBcc(), 'bcc'));
        }

        if ($message->getReplyTo()) {
            $data['headers'] = ['Reply-To' => $message->getReplyTo()->getEmail()];
        }

        if ($attachments = $message->getAttachments()) {
            foreach ($attachments as $attachment) {
                $data['attachments'][] = [
                    'type' => $attachment->getType(),
                    'name' => $attachment->getName(),
                    'content' => base64_encode($attachment->getContent())
                ];
            }
        }

        return $data;
    }

    /**
     * @param Mail[] $recipients
     * @param string $type
     * @return array
     */
    private function prepareRecipientsData($recipients, $type = 'to')
    {
        $data = [];
        foreach ($recipients as $recipient) {
            $data[] = [
                'email' => $recipient->getEmail(),
                'name' => $recipient->getName(),
                'type' => $type
            ];
        }

        return $data;
    }
}