<?php

namespace Deporvillage\NotificationService\Service\Mail;

use Deporvillage\NotificationService\Entity\Message\Message;

interface MailServiceInterface
{
    public function send(Message $message);
}