<?php

namespace Deporvillage\NotificationService\Service\Auth;

use Deporvillage\NotificationService\Service\Response;
use Firebase\JWT\JWT;
use Tuupola\Base62;
use Teapot\StatusCode;

class AuthService implements AuthServiceInterface
{
    /** @var array */
    protected $config;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @param array $params
     * @return Response
     * @throws \Exception
     */
    public function authenticate($params)
    {
        $this->validateTokenParams($params);

        $issuedAt = new \DateTime();
        $expire = new \DateTime('now ' . $this->config['tokenExpirationInterval']);

        $jsonTokenId = (new Base62)->encode(random_bytes(16));

        $payload = [
            "iat" => $issuedAt->getTimeStamp(),
            "exp" => $expire->getTimeStamp(),
            "jti" => $jsonTokenId,
        ];

        $token = JWT::encode($payload, $this->config['secret']);

        $data["token"] = $token;
        $data["expires"] = $expire->getTimeStamp();

        return new Response(StatusCode::CREATED, 'Token created successfully', $data);
    }

    /**
     * @param $params
     * @return bool
     * @throws \Exception
     */
    public function validateTokenParams($params)
    {
        if ($params['email'] !== $this->config['tokenAuthUser'] ||
            $params['password'] !== $this->config['tokenAuthPass']
        ) {
            throw new \Exception('Invalid email or password', StatusCode::BAD_REQUEST);
        }

        return true;
    }
}