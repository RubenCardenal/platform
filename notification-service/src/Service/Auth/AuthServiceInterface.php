<?php

namespace Deporvillage\NotificationService\Service\Auth;

use Deporvillage\NotificationService\Service\Response;

interface AuthServiceInterface
{
    /**
     * @param array $params
     * @return Response
     */
    public function authenticate($params);
}