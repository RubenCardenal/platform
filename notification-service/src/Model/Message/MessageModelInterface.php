<?php

namespace Deporvillage\NotificationService\Model\Message;

use Deporvillage\NotificationService\Entity\Attachment\Attachment;
use Deporvillage\NotificationService\Entity\Message\Message;

interface MessageModelInterface
{
    /**
     * @param Message $message
     * @return Message
     */
    public function save(Message $message);

    /**
     * @param int $messageId
     * @return Message
     */
    public function findById($messageId);

    /**
     * @param $messageId
     * @return bool
     */
    public function exists($messageId);

    /**
     * @param int $messageId
     * @param Attachment $attachment
     * @return bool
     */
    public function uploadAttachment($messageId, Attachment $attachment);
}