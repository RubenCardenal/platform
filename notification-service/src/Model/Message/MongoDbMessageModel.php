<?php

namespace Deporvillage\NotificationService\Model\Message;

use MongoDB\Collection;
use Deporvillage\NotificationService\Entity\Message\Message;
use Deporvillage\NotificationService\Entity\Attachment\Attachment;

class MongoDbMessageModel implements MessageModelInterface
{

    /**
     * @var \MongoDB\Collection
     */
    private $collection;

    public function __construct(\MongoDB\Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Message $message)
    {
        $phpMessageObject = json_decode(json_encode($message)); //Hacky but works rather than doing recursive jsonEncode invocations
        $insertOneResult = $this->collection->insertOne($phpMessageObject);
        return $message;

    }

    /**
     * @inheritdoc
     */
    public function findById($messageId)
    {

    }

    /**
     * @inheritdoc
     */
    public function exists($messageId)
    {

    }

    /**
     * @inheritdoc
     */
    public function uploadAttachment($messageId, Attachment $attachment)
    {

    }
}