<?php

namespace Deporvillage\NotificationService\Model\Message;

use Deporvillage\NotificationService\Entity\Message\Message;
use Deporvillage\NotificationService\Entity\Attachment\Attachment;

/**
 * Dummy Active Model Record class that doesn't do any persistence
 * This should be used for mocks/devs not requiring any persistence
 * This was originally created as a stop gap from MySQL to MongoDB
 */
class DummyMessageModel implements MessageModelInterface
{
    /**
     * @inheritdoc
     */
    public function save(Message $message)
    {

    }

    /**
     * @inheritdoc
     */
    public function findById($messageId)
    {

    }

    /**
     * @inheritdoc
     */
    public function exists($messageId)
    {

    }

    /**
     * @inheritdoc
     */
    public function uploadAttachment($messageId, Attachment $attachment)
    {

    }
}