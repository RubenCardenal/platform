<?php

namespace Deporvillage\NotificationService\Model\Message;

use Deporvillage\NotificationService\Entity\Attachment\Attachment;
use Deporvillage\NotificationService\Entity\Message\Message;
use PDO;

class MysqlMessageModel implements MessageModelInterface
{
    const TABLE_NAME_MESSAGES = 'messages';
    const TABLE_NAME_ATTACHMENTS = 'attachments';

    /** @var PDO */
    protected $database;

    public function __construct($database)
    {
        $this->database = $database;
    }

    /**
     * {@inheritdoc}
     */
    public function save(Message $message)
    {
        $stmt = $this->database->prepare(
            "INSERT INTO " . self::TABLE_NAME_MESSAGES . " (message) VALUES (?)"
        );

        $result = $stmt->execute([serialize($message)]);
        if ($result) {
            $message->setId($this->database->lastInsertId());
        }

        return $message;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($messageId)
    {
        $stmt = $this->database->prepare(
            "DELETE FROM " . self::TABLE_NAME_MESSAGES . " WHERE id = ?"
        );

        return $stmt->execute([$messageId]);
    }

    /**
     * {@inheritdoc}
     */
    public function findById($messageId)
    {
        $stmt = $this->database->prepare(
            "SELECT message FROM " . self::TABLE_NAME_MESSAGES . " WHERE id = ?"
        );

        $stmt->execute([$messageId]);
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$row) {
            return false;
        }

        /** @var Message $message */
        $message = unserialize($row['message']);

        $stmt = $this->database->prepare(
            "SELECT * FROM " . self::TABLE_NAME_ATTACHMENTS . " WHERE message_id = ?"
        );

        $stmt->execute([$messageId]);
        $rows = $stmt->fetchAll();

        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $attachment = new Attachment($row['type'], $row['name'], $row['content']);
                $message->addAttachment($attachment);
            }
        }

        return $message;
    }

    /**
     * {@inheritdoc}
     */
    public function exists($messageId)
    {
        $stmt = $this->database->prepare(
            "SELECT id FROM " . self::TABLE_NAME_MESSAGES . " WHERE id = ?"
        );

        $stmt->execute([$messageId]);

        return (bool)$stmt->fetchColumn();
    }

    /**
     * {@inheritdoc}
     */
    public function uploadAttachment($messageId, Attachment $attachment)
    {
        $stmt = $this->database->prepare(
            "INSERT INTO " . self::TABLE_NAME_ATTACHMENTS . " (message_id, type, name, content) VALUES (?, ?, ?, ?)"
        );

        return $stmt->execute(
            [
                $messageId,
                $attachment->getType(),
                $attachment->getName(),
                $attachment->getContent()
            ]
        );
    }
}