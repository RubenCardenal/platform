<?php

namespace Deporvillage\NotificationService\Handler;

use Psr\Http\Message\ServerRequestInterface;
use Deporvillage\NotificationService\Service\Response;
use Psr\Http\Message\ResponseInterface;
use Slim\Handlers\Error;
use Teapot\StatusCode;

class ApiErrorHandler extends Error
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, \Exception $exception)
    {
        $code = !empty($exception->getCode()) ? $exception->getCode() : StatusCode::INTERNAL_SERVER_ERROR;

        $result = new Response($code, $exception->getMessage());

        return $response->withJson($result, $result->code);
    }
}