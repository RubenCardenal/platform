<?php

namespace Deporvillage\NotificationService\Controller;

use Deporvillage\NotificationService\Service\Auth\AuthService;
use Monolog\Logger;
use Slim\Http\Request;
use Slim\Http\Response;

class AuthController extends Controller
{
    /** @var  AuthService */
    protected $authService;

    public function __construct(
        AuthService $authService,
        Logger $logger
    ) {
        $this->authService = $authService;
        $this->logger = $logger;
    }

    /**
     * @return Response
     * @SWG\Post(
     *     path="/v1/auth/token",
     *     tags={"Auth"},
     *     summary="Create authentication token",
     *     operationId="Token",
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="",
     *         in="body",
     *         name="body",
     *         required=true,
     *         @SWG\Schema(
     *             title="AuthEntity",
     *             required={"email", "password"},
     *             @SWG\Property(
     *                 property="email",
     *                 type="string",
     *                 example="string"
     *             ),
     *             @SWG\Property(
     *                 property="password",
     *                 type="string",
     *                 format="password",
     *                 example="string"
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Created",
     *         @SWG\Schema(
     *             title="Response",
     *             @SWG\Property(
     *                 property="error",
     *                 type="boolean",
     *                 example=false
     *             ),
     *             @SWG\Property(
     *                 property="code",
     *                 type="int",
     *                 example=201
     *             ),
     *             @SWG\Property(
     *                 property="message",
     *                 type="string",
     *                 example="Token created successfully"
     *             ),
     *             @SWG\Property(
     *                 property="data",
     *                 title="Token",
     *                 type="object",
     *                 @SWG\Property(
     *                     property="token",
     *                     description="Access token",
     *                     type="string",
     *                     example="eyJhbGciOiJIUzIsgjGJnR5cCI6IkpXVC..."
     *                 ),
     *                 @SWG\Property(
     *                     property="expires",
     *                     description="Expiration time (timestamp)",
     *                     type="integer",
     *                     example="426256373"
     *                 ),
     *             ),
     *         ),
     *     ),
     *     @SWG\Response(response="400", ref="#/responses/400BadRequest"),
     * )
     */
    public function setToken(Request $request, Response $respose)
    {
        $params = array(
            'email' => $request->getParsedBodyParam('email'),
            'password' => $request->getParsedBodyParam('password'),
        );

        $result = $this->authService->authenticate($params);

        return $this->renderResponse($respose, $result);
    }
}