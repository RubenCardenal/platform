<?php

namespace Deporvillage\NotificationService\Controller;

use Deporvillage\NotificationService\Service\Response as ServiceResponse;
use Monolog\Logger;
use Slim\Http\Response;

class Controller
{
    /** @var Logger */
    protected $logger;

    /**
     * @param Response $response
     * @param ServiceResponse $result
     * @return Response
     */
    protected function renderResponse(Response $response, ServiceResponse $result)
    {
        $response = $response->withJson($result, $result->code);

        $this->logger->info('RES: ' . $response->getStatusCode() . ':' . $response->getReasonPhrase());

        return $response;
    }
}
