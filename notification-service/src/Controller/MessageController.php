<?php

namespace Deporvillage\NotificationService\Controller;

use Deporvillage\NotificationService\Entity\Attachment\Attachment;
use Deporvillage\NotificationService\Entity\Message\MessageBuilder;
use Deporvillage\NotificationService\Service\Message\MessageServiceInterface as MessageService;
use Psr\Http\Message\UploadedFileInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Log\LoggerInterface as Logger;

class MessageController extends Controller
{
    const TYPE_DEFAULT = 'default';
    const ACTION_SAVE = 'save';
    const ACTION_SEND = 'send';

    /**
     * @var MessageService
     */
    protected $messageService;

    /**
     * @var MessageBuilder
     */
    protected $messageBuilder;

    public function __construct(MessageService $messageService, MessageBuilder $messageBuilder, Logger $logger)
    {
        $this->messageService = $messageService;
        $this->messageBuilder = $messageBuilder;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param string $messageType
     * @param string $messageAction
     * @return Response
     */
    protected function execute(
        Request $request,
        Response $response,
        $messageType = self::TYPE_DEFAULT,
        $messageAction = self::ACTION_SEND
    ) {
        $this->logger->info('REQ: ' . $request->getUri());

        $message = $this->messageBuilder->build($request, $messageType);


        $result = $this->messageService->save($message);
        //$result = $this->messageService->send($message);
    

        return $this->renderResponse($response, $result);
    }

    /**
     * @SWG\Post(
     *     path="/v1/message",
     *     tags={"Message"},
     *     summary="Send a message",
     *     operationId="generic",
     *     produces={"application/json"},
     *     @SWG\Parameter(ref="#/parameters/X-Token"),
     *     @SWG\Parameter(ref="#/parameters/Accept-Language"),
     *     @SWG\Parameter(
     *         description="",
     *         in="body",
     *         name="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/DefaultMessage")
     *     ),
     *     @SWG\Response(response="200", ref="#/responses/200OK"),
     *     @SWG\Response(response="400", ref="#/responses/400BadRequest"),
     *     @SWG\Response(response="401", ref="#/responses/401Unauthorized"),
     *     @SWG\Response(response="500", ref="#/responses/500InternalServerError")
     * )
     */
    public function generic(Request $request, Response $response)
    {
        return $this->execute($request, $response);
    }

    /**
     * @SWG\Post(
     *     path="/v1/message/draft",
     *     tags={"Draft"},
     *     summary="Create a draft message",
     *     operationId="genericDraft",
     *     produces={"application/json"},
     *     @SWG\Parameter(ref="#/parameters/X-Token"),
     *     @SWG\Parameter(ref="#/parameters/Accept-Language"),
     *     @SWG\Parameter(
     *         description="",
     *         in="body",
     *         name="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/DefaultMessage")
     *     ),
     *     @SWG\Response(response="201", ref="#/responses/201Created"),
     *     @SWG\Response(response="400", ref="#/responses/400BadRequest"),
     *     @SWG\Response(response="401", ref="#/responses/401Unauthorized"),
     *     @SWG\Response(response="500", ref="#/responses/500InternalServerError")
     * )
     */
    public function genericDraft(Request $request, Response $response)
    {
        return $this->execute($request, $response, self::TYPE_DEFAULT, self::ACTION_SAVE);
    }

    /**
     * @SWG\Post(
     *     path="/v1/message/newAccount",
     *     tags={"Message"},
     *     summary="Send a new account message",
     *     operationId="newAccount",
     *     produces={"application/json"},
     *     @SWG\Parameter(ref="#/parameters/X-Token"),
     *     @SWG\Parameter(ref="#/parameters/Accept-Language"),
     *     @SWG\Parameter(
     *         description="",
     *         in="body",
     *         name="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/NewAccountMessage")
     *     ),
     *     @SWG\Response(response="200", ref="#/responses/200OK"),
     *     @SWG\Response(response="400", ref="#/responses/400BadRequest"),
     *     @SWG\Response(response="401", ref="#/responses/401Unauthorized"),
     *     @SWG\Response(response="500", ref="#/responses/500InternalServerError")
     * )
     */
    public function newAccount(Request $request, Response $response)
    {
        return $this->execute($request, $response, 'newAccount');
    }

    /**
     * @SWG\Post(
     *     path="/v1/message/orderConfirmation",
     *     tags={"Message"},
     *     summary="Send an order confirmation message",
     *     operationId="orderConfirmation",
     *     produces={"application/json"},
     *     @SWG\Parameter(ref="#/parameters/X-Token"),
     *     @SWG\Parameter(ref="#/parameters/Accept-Language"),
     *     @SWG\Parameter(
     *         description="",
     *         in="body",
     *         name="body",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/OrderConfirmationMessage")
     *     ),
     *     @SWG\Response(response="200", ref="#/responses/200OK"),
     *     @SWG\Response(response="400", ref="#/responses/400BadRequest"),
     *     @SWG\Response(response="401", ref="#/responses/401Unauthorized"),
     *     @SWG\Response(response="500", ref="#/responses/500InternalServerError")
     * )
     */
    public function orderConfirmation(Request $request, Response $response)
    {
        return $this->execute($request, $response, 'orderConfirmation');
    }

    /**
     * @SWG\Post(
     *     path="/v1/message/{messageId}/uploadAttachment",
     *     tags={"Draft"},
     *     summary="Upload attachment",
     *     operationId="attachment",
     *     consumes={"multipart/form-data"},
     *     produces={"application/json"},
     *     @SWG\Parameter(ref="#/parameters/X-Token"),
     *     @SWG\Parameter(ref="#/parameters/messageId"),
     *     @SWG\Parameter(ref="#/parameters/attachment"),
     *     @SWG\Response(response="200", ref="#/responses/200OK"),
     *     @SWG\Response(response="400", ref="#/responses/400BadRequest"),
     *     @SWG\Response(response="401", ref="#/responses/401Unauthorized"),
     *     @SWG\Response(response="404", ref="#/responses/404NotFound"),
     *     @SWG\Response(response="500", ref="#/responses/500InternalServerError")
     * )
     */
    public function uploadAttachment(Request $request, Response $response, $args)
    {
        $this->logger->info('REQ: ' . $request->getUri());

        /** @var UploadedFileInterface $uploadedFile */
        $uploadedFile = $request->getUploadedFiles()['attachment'];

        $attachment = new Attachment(
            $uploadedFile->getClientMediaType(),
            $uploadedFile->getClientFilename(),
            $uploadedFile->getStream()->getContents()
        );
        $result = $this->messageService->uploadAttachment($args['messageId'], $attachment);

        return $this->renderResponse($response, $result);
    }

    /**
     * @SWG\Post(
     *     path="/v1/message/{messageId}/send",
     *     tags={"Draft"},
     *     summary="Send a draft message",
     *     operationId="send",
     *     produces={"application/json"},
     *     @SWG\Parameter(ref="#/parameters/X-Token"),
     *     @SWG\Parameter(ref="#/parameters/messageId"),
     *     @SWG\Response(response="200", ref="#/responses/200OK"),
     *     @SWG\Response(response="400", ref="#/responses/400BadRequest"),
     *     @SWG\Response(response="401", ref="#/responses/401Unauthorized"),
     *     @SWG\Response(response="404", ref="#/responses/404NotFound"),
     *     @SWG\Response(response="500", ref="#/responses/500InternalServerError")
     * )
     */
    public function send(Request $request, Response $response, $args)
    {
        $this->logger->info('REQ: ' . $request->getUri());

        $result = $this->messageService->sendById($args['messageId']);

        return $this->renderResponse($response, $result);
    }
}
