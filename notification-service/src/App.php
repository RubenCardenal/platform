<?php

namespace Deporvillage\NotificationService;

use PDO;
use Mandrill;
use Monolog\Logger;
use Slim\Views\Twig;
use Teapot\StatusCode;
use MongoDB\Collection;
use Slim\Views\TwigExtension;
use Monolog\Handler\StreamHandler;
use Monolog\Processor\UidProcessor;
use Slim\Middleware\JwtAuthentication;
use Deporvillage\NotificationService\Entity\Token\Token;
use Deporvillage\NotificationService\Handler\ApiErrorHandler;
use Deporvillage\NotificationService\Service\Auth\AuthService;
use Deporvillage\NotificationService\Controller\AuthController;
use Deporvillage\NotificationService\Controller\MessageController;
use Deporvillage\NotificationService\Entity\Message\MessageBuilder;
use Deporvillage\NotificationService\Service\Message\MessageService;
use Deporvillage\NotificationService\Model\Message\DummyMessageModel;
use Deporvillage\NotificationService\Model\Message\MysqlMessageModel;
use Deporvillage\NotificationService\Service\Mail\MandrillMailService;
use Deporvillage\NotificationService\Model\Message\MongoDbMessageModel;
use Deporvillage\NotificationService\Service\Validator\ValidatorService;

class App extends \Slim\App
{
    /**
     * @param bool $withMiddleware
     * @return $this
     */
    public function init($withMiddleware = true)
    {
        $this->initServices();
        if ($withMiddleware) {
            $this->initMiddlewares();
        }
        $this->initHandlers();
        $this->initRoutes();

        return $this;
    }

    protected function initServices()
    {
        $container = $this->getContainer();



        $container['view'] = function () use ($container) {
            $settings = $container->get('settings')['view'];
            $view = new Twig($settings['template_path'], $settings['template_settings']);
            $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
            $view->addExtension(new TwigExtension($container['router'], $basePath));

            return $view;
        };

        $container[Logger::class] = function () use ($container) {
            $settings = $container->get('settings')['logger'];
            $logger = new Logger($settings['name']);
            $logger->pushProcessor(new UidProcessor());
            $logger->pushHandler(new StreamHandler($settings['path'], $settings['level']));

            return $logger;
        };

        $container[Mandrill::class] = function () use ($container) {
            $settings = $container->get('settings')['mandrill'];
            $mandrill = new Mandrill($settings['apikey']);

            return $mandrill;
        };

        $container[ValidatorService::class] = function () use ($container) {
            return new ValidatorService();
        };

        // MySQL Crap
        // $container[PDO::class] = function () use ($container) {
        //     $db = $container->get('settings')['db'];
        //     $pdo = new PDO("mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'], $db['user'], $db['pass']);
        //     $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //     $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

        //     return $pdo;
        // };
        // $container[MysqlMessageModel::class] = function () use ($container) {
        //     return new MysqlMessageModel($container[PDO::class]);
        // };

        //Mongo Collection
        $container[\MongoDB\Collection::class] = function () use ($container) {
            $mongoSettings = $container->get('settings')['mongodb'];
            $mongoUri = "mongodb://" . $mongoSettings['user'] . ":" . $mongoSettings['pass'] . "@" . $mongoSettings['host'] . ":" . $mongoSettings['port'];

            $client = new \MongoDB\Client($mongoUri);
            $database = $client->selectDatabase('notification-service');
            return $database->selectCollection('messages');
        };

        $container[MongoDbMessageModel::class] = function () use ($container) {
            return new MongoDbMessageModel($container[\MongoDB\Collection::class]);
        };

        $container[DummyMessageModel::class] = function () use ($container) {
            return new DummyMessageModel();
        };

        $container[MandrillMailService::class] = function () use ($container) {
            return new MandrillMailService($container[Mandrill::class]);
        };

        $container[MessageService::class] = function () use ($container) {
            return new MessageService(
                $container[ValidatorService::class],
                $container[MongoDbMessageModel::class],
                $container[MandrillMailService::class],
                $container['view'],
                $container[Logger::class]
            );
        };

        $container[MessageBuilder::class] = function () use ($container) {
            $config = $container->get('settings')['messages'];
            return new MessageBuilder($config);
        };

        $container[MessageController::class] = function () use ($container) {
            return new MessageController(
                $container[MessageService::class],
                $container[MessageBuilder::class],
                $container[Logger::class]
            );
        };

        $container[AuthService::class] = function () use ($container) {
            $config = $container->get('settings')['authentication'];
            return new AuthService($config);
        };

        $container[AuthController::class] = function () use ($container) {
            return new AuthController(
                $container[AuthService::class],
                $container[Logger::class]
            );
        };
    }

    protected function initMiddlewares()
    {
        $container = $this->getContainer();

        $container["token"] = function ($container) {
            return new Token();
        };

        $container[JwtAuthentication::class] = function () use ($container) {
            $options = $container->get('settings')['authentication'];
            $options["error"] = function ($request, $response, $args) {
                throw new \Exception('Authentication failed: ' . $args['message'], StatusCode::UNAUTHORIZED);
            };
            $options["before"] = function ($request, $response, $args) use ($container) {
                $container["token"]->hydrate($args["decoded"]);
            };

            return new JwtAuthentication($options);
        };

        $this->add(JwtAuthentication::class);
    }

    protected function initHandlers()
    {
        $container = $this->getContainer();

        # Override slim error handler
        $container['errorHandler'] = function ($container) {
            return new ApiErrorHandler(
                $container[Logger::class]
            );
        };

        # Override slim not found error handler
        $container['notFoundHandler'] = function () {
            return function () {
                throw new \Exception('Route not found', StatusCode::NOT_FOUND);
            };
        };

        # Override slim not allowed error handler
        $container['notAllowedHandler'] = function () {
            return function ($request, $response, $methods) {
                throw new \Exception(
                    'Method not allowed, must be one of: ' . implode(', ', $methods),
                    StatusCode::METHOD_NOT_ALLOWED
                );
            };
        };
    }

    protected function initRoutes()
    {
        # api swagger documentation
        $this->get('/v1/swagger-json', function ($request, $response, $args) {
            $swagger = \Swagger\scan(__DIR__ . '/../src');
            return $response->withJson($swagger, 201);
        });

        $this->get('/', function ($request, $response, $args) {
            return $this->view->render($response, 'index.html.twig', $args);
        });

        # Authentication
        $this->group('/v1/auth', function () {
            # Set access token
            $this->post('/token', AuthController::class . ':setToken');
        });

        $this->group('/v1/message', function () {
            /** Create and send messages */
            $this->post('', MessageController::class . ':generic');
            $this->post('/newAccount', MessageController::class . ':newAccount');
            $this->post('/orderConfirmation', MessageController::class . ':orderConfirmation');

            /** Draft messages */
            $this->post('/draft', MessageController::class . ':genericDraft');
            $this->post('/{messageId}/uploadAttachment', MessageController::class . ':uploadAttachment');
            $this->post('/{messageId}/send', MessageController::class . ':send');
        });
    }
}
