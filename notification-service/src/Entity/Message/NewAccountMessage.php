<?php

namespace Deporvillage\NotificationService\Entity\Message;

/**
 * @SWG\Definition(
 *     required={
 *         "to",
 *         "templateVars",
 *     }
 * )
 */
class NewAccountMessage extends Message
{
    /**
     * @SWG\Property(readOnly=true)
     */
    protected $subject;

    /**
     * @SWG\Property(readOnly=true)
     */
    protected $from;

    /**
     * @SWG\Property(readOnly=true)
     */
    protected $cc;

    /**
     * @SWG\Property(readOnly=true)
     */
    protected $bcc;

    /**
     * @SWG\Property(readOnly=true)
     */
    protected $replyTo;

    /**
     * @SWG\Property(
     *     property="templateVars",
     *     description="Variables needed for template substitution",
     *     required={"customer"},
     *     @SWG\Property(
     *         property="customer",
     *         required={"name","email","password"},
     *         @SWG\Property(
     *             property="name",
     *             type="string",
     *             example="Customer Name",
     *         ),
     *         @SWG\Property(
     *             property="email",
     *             type="email",
     *             example="customer@deporvillage.com",
     *         ),
     *         @SWG\Property(
     *             property="password",
     *             type="password",
     *             example="P64xTNTc",
     *         )
     *     ),
     * )
     */
    protected $templateVars;
}