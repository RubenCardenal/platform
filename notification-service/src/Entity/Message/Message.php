<?php

namespace Deporvillage\NotificationService\Entity\Message;

use Deporvillage\NotificationService\Entity\Attachment\Attachment;
use Deporvillage\NotificationService\Entity\Mail\Mail;

/**
 * @SWG\Definition(
 *     required={
 *         "from",
 *         "to",
 *         "subject",
 *         "templateVars"
 *     }
 * )
 */
abstract class Message implements \JsonSerializable
{
    /** @var int */
    protected $id;

    /**
     * @var string
     */
    protected $language;

    /**
     * @var Mail
     * @SWG\Property(ref="#/definitions/Mail")
     */
    protected $from;

    /**
     * @var Mail[]
     * @SWG\Property(
     *     property="to",
     *     type="array",
     *     minItems=1,
     *     @SWG\Items(ref="#/definitions/Mail"),
     * )
     */
    protected $to;

    /**
     * @var Mail[]
     * @SWG\Property(
     *     property="cc",
     *     type="array",
     *     minItems=1,
     *     @SWG\Items(ref="#/definitions/Mail"),
     * )
     */
    protected $cc;

    /**
     * @var Mail[]
     * @SWG\Property(
     *     property="bcc",
     *     type="array",
     *     minItems=1,
     *     @SWG\Items(ref="#/definitions/Mail"),
     * )
     */
    protected $bcc;

    /**
     * @var Mail
     * @SWG\Property(ref="#/definitions/Mail")
     */
    protected $replyTo;

    /**
     * @var string
     * @SWG\Property(
     *     property="subject",
     *     type="string",
     *     example="Welcome to deporvillage.com"
     * )
     */
    protected $subject;

    /**
     * @var string
     */
    protected $templatePath;

    /**
     * @var array
     */
    protected $templateVars;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var Attachment[]
     */
    protected $attachments;

    /**
     * @var array
     */
    protected $requiredFields = [];

    /**
     * @param string $language
     */
    public function __construct($language = 'es-ES')
    {
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return Mail
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param Mail $from
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return Mail[]
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param Mail[] $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;

        return $this;
    }

    /**
     * @return Mail[]
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @param Mail[] $cc
     * @return $this
     */
    public function setCc($cc)
    {
        $this->cc = $cc;

        return $this;
    }

    /**
     * @return Mail[]
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param Mail[] $bcc
     * @return $this
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }

    /**
     * @return Mail
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @param Mail $replyTo
     * @return $this
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;

        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplatePath()
    {
        return $this->templatePath;
    }

    /**
     * @param string $templatePath
     * @return Message
     */
    public function setTemplatePath($templatePath)
    {
        $this->templatePath = $templatePath;

        return $this;
    }

    /**
     * @return array
     */
    public function getTemplateVars()
    {
        return $this->templateVars;
    }

    /**
     * @param array $templateVars
     * @return $this
     */
    public function setTemplateVars($templateVars)
    {
        $this->templateVars = $templateVars;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Attachment[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param Attachment $attachment
     */
    public function addAttachment(Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    }

    /**
     * @param array $requiredFields
     * @return Message
     */
    public function setRequiredFields($requiredFields)
    {
        $this->requiredFields = $requiredFields;

        return $this;
    }

    /**
     * @param string $field
     * @return bool
     */
    public function isRequired($field)
    {
        return in_array($field, $this->requiredFields);
    }

    public function jsonSerialize()
    {
        $json = array();
        foreach($this as $key => $value) {
            $json[$key] = $value;
        }
        $json['timestamp'] = date_timestamp_get(date_create());
        return $json;
    }
}