<?php

namespace Deporvillage\NotificationService\Entity\Message;

/**
 * @SWG\Definition(
 *     required={
 *         "from",
 *         "to",
 *         "subject",
 *         "templateVars"
 *     }
 * )
 */
class DefaultMessage extends Message
{
    /**
     * @SWG\Property(
     *     property="templateVars",
     *     description="Variables needed for template substitution",
     *     required={"content"},
     *     @SWG\Property(
     *         property="content",
     *         type="string",
     *         description="Message content in html format",
     *         example="<html><body>Content in <strong>html</strong> format...</body></html>"
     *     )
     * )
     */
    protected $templateVars;
}