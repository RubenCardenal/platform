<?php

namespace Deporvillage\NotificationService\Entity\Message;

/**
 * @SWG\Definition(
 *     required={
 *         "to",
 *         "templateVars",
 *     }
 * )
 */
class OrderConfirmationMessage extends Message
{
    /**
     * @SWG\Property(readOnly=true)
     */
    protected $subject;

    /**
     * @SWG\Property(readOnly=true)
     */
    protected $from;

    /**
     * @SWG\Property(readOnly=true)
     */
    protected $cc;

    /**
     * @SWG\Property(readOnly=true)
     */
    protected $bcc;

    /**
     * @SWG\Property(readOnly=true)
     */
    protected $replyTo;

    /**
     * @SWG\Property(
     *     property="templateVars",
     *     description="Variables needed for template substitution",
     *     required={"incrementId"},
     *     @SWG\Property(
     *         property="incrementId",
     *         type="int",
     *         description="Increment Id",
     *         example="100651316"
     *     )
     * )
     */
    protected $templateVars;
}