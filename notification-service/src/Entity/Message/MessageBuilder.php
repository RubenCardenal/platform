<?php

namespace Deporvillage\NotificationService\Entity\Message;

use Deporvillage\NotificationService\Entity\Mail\Mail;
use Slim\Http\Request;

class MessageBuilder
{
    /** @var array */
    protected $config;

    /**
     * @param array $config
     */
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * @param Request $request
     * @param string $messageType
     * @return Message
     */
    public function build(Request $request, $messageType)
    {
        $config = $this->config[$messageType];
        $defaultFrom = $this->config['defaultFrom'];
        $allowedFields = $config['fields']['required'];
        if (isset($config['fields']['optional'])) {
            $allowedFields = array_merge($allowedFields, $config['fields']['optional']);
        }

        if (in_array('from', $config['fields']['required'])) {
            $from = $this->createMail($request->getParsedBodyParam('from'));
        } elseif (isset($config['from'])) {
            $from = $this->createMail($config['from']);
        } else {
            $from = $this->createMail($defaultFrom);
        }

        $to = $this->createMailList($request->getParsedBodyParam('to'));

        if ($request->getParsedBodyParam('cc') && in_array('cc', $allowedFields)) {
            $cc = $this->createMailList($request->getParsedBodyParam('cc'));
        }

        if ($request->getParsedBodyParam('bcc') && in_array('bcc', $allowedFields)) {
            $bcc = $this->createMailList($request->getParsedBodyParam('bcc'));
        }

        if ($request->getParsedBodyParam('replyTo') && in_array('replyTo', $allowedFields)) {
            $replyTo = $this->createMail($request->getParsedBodyParam('replyTo'));
        }

        $language = $request->getHeader('Accept-Language')[0];
        if (isset($config['templateSubjects'][$language])) {
            $subject = $config['templateSubjects'][$language];
        } else {
            $subject = $request->getParsedBodyParam('subject');
        }

        $templatePath = 'message/' . $language . '/' . $config['templateFile'];

        $class = 'Deporvillage\\NotificationService\\Entity\\Message\\' . $config['class'];

        /** @var Message $message */
        $message = new $class($language);
        $message
            ->setFrom($from)
            ->setTo($to)
            ->setSubject($subject)
            ->setTemplatePath($templatePath)
            ->setTemplateVars($request->getParsedBodyParam('templateVars'))
            ->setRequiredFields($config['fields']['required']);

        if (isset($cc)) {
            $message->setCc($cc);
        }
        if (isset($bcc)) {
            $message->setBcc($bcc);
        }
        if (isset($replyTo)) {
            $message->setReplyTo($replyTo);
        }

        return $message;
    }

    /**
     * @param array $param
     * @return Mail[]
     */
    private function createMailList($param)
    {
        $mails = [];
        foreach ($param as $item) {
            $mail = $this->createMail($item);
            if (!is_null($mail)) {
                $mails[] = $mail;
            }
        }

        return $mails;
    }

    /**
     * @param array $data
     * @return Mail
     */
    private function createMail($data)
    {
        if (!isset($data['email'])) {
            return null;
        }

        return new Mail($data['email'], $data['name']);
    }
}