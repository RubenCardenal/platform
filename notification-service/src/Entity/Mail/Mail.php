<?php

namespace Deporvillage\NotificationService\Entity\Mail;

/**
 * @SWG\Definition(
 *     definition="Mail",
 *     required={"email"}
 * )
 */
class Mail implements \JsonSerializable
{
    /**
     * @var string
     * @SWG\Property(
     *     property="email",
     *     type="email",
     *     example="no-reply@deporvillage.com"
     *  )
     */
    private $email;

    /**
     * @var string
     * @SWG\Property(
     *     property="name",
     *     type="string",
     *     example="Deporvillage.com"
     *  )
     */
    private $name;

    /**
     * @param string $email
     * @param string $name
     */
    public function __construct($email, $name = '')
    {
        $this->email = $email;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Method that is used by json_encode/json_decode
     * PHP functions
     */
    public function jsonSerialize()
    {
        $json = array();
        foreach($this as $key => $value) {
            $json[$key] = $value;
        }
        return $json;
    }
}