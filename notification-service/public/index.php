<?php

$env = getenv('APP_ENV');
if (empty($env)) {
    $env = 'prod';
}

define("API_HOST", ($env === 'dev') ? 'localhost:8086' : 'api-mailing.deporvillage.com');

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

require __DIR__ . '/../vendor/autoload.php';

session_start();

$settings = require __DIR__ . '/../config/settings_' . $env . '.php';
$app = new \Deporvillage\NotificationService\App($settings);

$app->init()->run();
