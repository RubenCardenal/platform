<?php

namespace Tests\Service\Auth;

use Deporvillage\NotificationService\Entity\Mail\Mail;
use Deporvillage\NotificationService\Entity\Message\DefaultMessage;
use Deporvillage\NotificationService\Service\Validator\ValidatorService;
use PHPUnit\Framework\TestCase;
use Teapot\StatusCode;

class ValidatorServiceTest extends TestCase
{
    /** @var ValidatorService */
    protected $validator;

    /** @var Mail */
    protected $validEmail;

    /** @var Mail */
    protected $invalidEmail;

    protected function setUp()
    {
        $this->validator = new ValidatorService();
        $this->validEmail = new Mail('no-reply@deporvillage.com', 'Deporvillage.com');
        $this->invalidEmail = new Mail('invalidEmail');
    }

    public function testInvalidLanguageThrowsException()
    {
        $this->expectBadRequestException('Field Accept-Language must be one of es-ES|fr-FR|it-IT|pt-PT');

        $message = new DefaultMessage('en-US');

        $this->validator->validate($message);
    }

    public function testRequiredAndEmptyFromThrowsException()
    {
        $this->expectBadRequestException('Field <from> is required');

        $message = new DefaultMessage();
        $message->setRequiredFields(['from']);

        $this->validator->validate($message);
    }

    public function testRequiredAndInvalidFromThrowsException()
    {
        $this->expectBadRequestException('Field <from> email is not valid');

        $message = new DefaultMessage();
        $message->setRequiredFields(['from']);
        $message->setFrom($this->invalidEmail);

        $this->validator->validate($message);
    }

    public function testRequiredAndEmptyToThrowsException()
    {
        $this->expectBadRequestException('Field <to> must be an array');

        $message = new DefaultMessage();
        $message->setRequiredFields(['to']);

        $this->validator->validate($message);
    }

    public function testRequiredAndInvalidToThrowsException()
    {
        $this->expectBadRequestException('Field <to> email is not valid');

        $message = new DefaultMessage();
        $message->setRequiredFields(['to']);
        $message->setTo([$this->invalidEmail]);

        $this->validator->validate($message);
    }

    public function testRequiredAndEmptyCcThrowsException()
    {
        $this->expectBadRequestException('Field <cc> must be an array');

        $message = new DefaultMessage();
        $message->setRequiredFields(['cc']);

        $this->validator->validate($message);
    }

    public function testRequiredAndInvalidCcThrowsException()
    {
        $this->expectBadRequestException('Field <cc> email is not valid');

        $message = new DefaultMessage();
        $message->setRequiredFields(['cc']);
        $message->setCc([$this->invalidEmail]);

        $this->validator->validate($message);
    }

    public function testRequiredAndEmptyBccThrowsException()
    {
        $this->expectBadRequestException('Field <bcc> must be an array');

        $message = new DefaultMessage();
        $message->setRequiredFields(['bcc']);

        $this->validator->validate($message);
    }

    public function testRequiredAndInvalidBccThrowsException()
    {
        $this->expectBadRequestException('Field <bcc> email is not valid');

        $message = new DefaultMessage();
        $message->setRequiredFields(['bcc']);
        $message->setBcc([$this->invalidEmail]);

        $this->validator->validate($message);
    }

    public function testRequiredAndEmptyReplyThrowsException()
    {
        $this->expectBadRequestException('Field <replyTo> is required');

        $message = new DefaultMessage();
        $message->setRequiredFields(['replyTo']);

        $this->validator->validate($message);
    }

    public function testRequiredAndInvalidReplyToThrowsException()
    {
        $this->expectBadRequestException('Field <replyTo> email is not valid');

        $message = new DefaultMessage();
        $message->setRequiredFields(['replyTo']);
        $message->setReplyTo($this->invalidEmail);

        $this->validator->validate($message);
    }

    public function testRequiredAndEmptySubjectThrowsException()
    {
        $this->expectBadRequestException('Field <subject> is required');

        $message = new DefaultMessage();
        $message->setRequiredFields(['subject']);

        $this->validator->validate($message);
    }

    public function testRequiredAndEmptyTemplateVarsThrowsException()
    {
        $this->expectBadRequestException('Field <templateVars> is required');

        $message = new DefaultMessage();
        $message->setRequiredFields(['templateVars']);

        $this->validator->validate($message);
    }

    public function testValidMessageReturnsValidationSuccess()
    {
        $message = new DefaultMessage();
        $message
            ->setFrom($this->validEmail)
            ->setTo([$this->validEmail])
            ->setCc([$this->validEmail])
            ->setBcc([$this->validEmail])
            ->setReplyTo($this->validEmail)
            ->setSubject('subject')
            ->setTemplateVars(['content' => 'testing content...'])
            ->setRequiredFields(['from', 'to', 'cc', 'bcc', 'replyTo', 'subject', 'templateVars']);

        $result = $this->validator->validate($message);

        $this->assertTrue($result);
    }

    private function expectBadRequestException($message)
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionCode(StatusCode::BAD_REQUEST);
        $this->expectExceptionMessage($message);
    }
}
