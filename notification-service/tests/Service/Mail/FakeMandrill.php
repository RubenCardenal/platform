<?php

namespace Tests\Service\Mail;

class FakeMandrill extends \Mandrill
{
    public function __construct($messages)
    {
        $this->messages = $messages;
    }

    public function __destruct()
    {
    }
}