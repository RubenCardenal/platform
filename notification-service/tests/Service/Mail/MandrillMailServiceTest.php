<?php

namespace Tests\Service\Mail;

use Deporvillage\NotificationService\Entity\Attachment\Attachment;
use Deporvillage\NotificationService\Entity\Mail\Mail;
use Deporvillage\NotificationService\Entity\Message\DefaultMessage;
use Deporvillage\NotificationService\Entity\Message\Message;
use Deporvillage\NotificationService\Service\Mail\MandrillMailService;
use PHPUnit\Framework\TestCase;

class MandrillMailServiceTest extends TestCase
{
    /** @var Message */
    protected $message;

    protected function setUp()
    {
        $validEmail = new Mail('no-reply@deporvillage.com');
        $message = new DefaultMessage();
        $message
            ->setFrom($validEmail)
            ->setTo([$validEmail])
            ->setCc([$validEmail])
            ->setBcc([$validEmail])
            ->setReplyTo($validEmail)
            ->setSubject('subject')
            ->setTemplateVars(['content' => 'testing content...'])
            ->setRequiredFields(['from', 'to', 'subject', 'templateVars']);

        $message->addAttachment(new Attachment('test type', 'test name', 'test content'));

        $this->message = $message;
    }

    public function testSendIsOk()
    {
        $mandrillMessages = $this->createMock(\Mandrill_Messages::class);
        $mandrillMessages
            ->method('send')
            ->willReturn([0 => ['status' => MandrillMailService::MANDRILL_STATUS_SENT]]);

        $mandrill = new FakeMandrill($mandrillMessages);

        $mandrillMailService = new MandrillMailService($mandrill);

        $this->assertTrue($mandrillMailService->send($this->message));
    }

    public function testSendIsKo()
    {
        $mandrillMessages = $this->createMock(\Mandrill_Messages::class);
        $mandrillMessages
            ->method('send')
            ->willReturn([0 => ['status' => MandrillMailService::MANDRILL_STATUS_REJECTED]]);

        $mandrill = new FakeMandrill($mandrillMessages);

        $mandrillMailService = new MandrillMailService($mandrill);

        $this->assertNotTrue($mandrillMailService->send($this->message));
    }
}