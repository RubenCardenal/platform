<?php

namespace Tests\Service\Auth;

use Deporvillage\NotificationService\Service\Auth\AuthService;
use PHPUnit\Framework\TestCase;
use Teapot\StatusCode;

class AuthServiceTest extends TestCase
{
    /** @var AuthService */
    protected $authService;

    protected function setUp()
    {
        $config = [
            'secret' => 'secrettest',
            'tokenExpirationInterval' => '+1 day',
            'tokenAuthUser' => 'rightuser@deporvillage.com',
            'tokenAuthPass' => 'rightpass',
        ];

        $this->authService = new AuthService($config);
    }

    public function testTokenCreatedSuccessfully()
    {
        $params = [
            'email' => 'rightuser@deporvillage.com',
            'password' => 'rightpass'
        ];

        $response = $this->authService->authenticate($params);

        $this->assertFalse($response->error);
        $this->assertEquals(StatusCode::CREATED, $response->code);
        $this->assertNotNull($response->data);
        $this->assertNotEmpty($response->data->token);
        $this->assertGreaterThan(0, $response->data->expires);
    }

    public function testInvalidEmailParamThrowsException()
    {
        $this->expectBadRequestException();

        $params = [
            'email' => 'wronguser@deporvillage.com',
            'password' => 'rightpassword'
        ];

        $this->authService->authenticate($params);
    }

    public function testInvalidPasswordParamThrowsException()
    {
        $this->expectBadRequestException();

        $params = [
            'email' => 'rightuser@deporvillage.com',
            'password' => 'wrongpassword'
        ];

        $this->authService->authenticate($params);
    }

    private function expectBadRequestException()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionCode(StatusCode::BAD_REQUEST);
        $this->expectExceptionMessage('Invalid email or password');
    }
}
