<?php

namespace Tests\Service\Auth;

use Deporvillage\NotificationService\Entity\Attachment\Attachment;
use Deporvillage\NotificationService\Entity\Mail\Mail;
use Deporvillage\NotificationService\Entity\Message\DefaultMessage;
use Deporvillage\NotificationService\Entity\Message\Message;
use Deporvillage\NotificationService\Model\Message\MessageModel;
use Deporvillage\NotificationService\Model\Message\MysqlMessageModel;
use Deporvillage\NotificationService\Service\Mail\MailServiceInterface;
use Deporvillage\NotificationService\Service\Mail\MandrillMailService;
use Deporvillage\NotificationService\Service\Message\MessageService;
use Deporvillage\NotificationService\Service\Validator\ValidatorService;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Slim\Views\Twig;
use Teapot\StatusCode;

class MessageServiceTest extends TestCase
{
    const MESSAGE_ID = 1;

    /** @var ValidatorService */
    protected $validator;
    /** @var MessageModel */
    protected $messageModel;
    /** @var MailServiceInterface */
    protected $mailService;
    /** @var Twig */
    protected $view;
    /** @var Logger */
    protected $logger;
    /** @var Message */
    protected $message;
    /** @var Attachment */
    protected $attachment;

    protected function setUp()
    {
        $this->validator = new ValidatorService();
        $this->messageModel = $this->createMock(MysqlMessageModel::class);
        $this->mailService = $this->createMock(MandrillMailService::class);
        $this->view = $this->createMock(Twig::class);
        $this->logger = $this->createMock(Logger::class);

        $message = new DefaultMessage();
        $message
            ->setFrom(new Mail('no-reply@deporvillage.com'))
            ->setTo([new Mail('no-reply@deporvillage.com')])
            ->setSubject('subject')
            ->setTemplateVars(['content' => 'testing content...'])
            ->setRequiredFields(['from', 'to', 'subject', 'templateVars']);

        $this->message = $message;
        $this->attachment = new Attachment('test type', 'test name', 'test content');
    }

    public function testSendResponseIsOk()
    {
        $mailService = $this->createMock(MandrillMailService::class);
        $mailService
            ->method('send')
            ->willReturn(true);

        $messageService = $this->createMessageServiceMock(null, $mailService);

        $response = $messageService->send($this->message);

        $this->assertEquals(StatusCode::OK, $response->code);
        $this->assertEquals('Message was sent successfully', $response->message);
    }

    public function testSendResponseIsInternalServerError()
    {
        $mailService = $this->createMock(MandrillMailService::class);
        $mailService
            ->method('send')
            ->will($this->throwException(new \Exception('Test')));

        $messageService = $this->createMessageServiceMock(null, $mailService);

        $response = $messageService->send($this->message);

        $this->assertEquals(StatusCode::INTERNAL_SERVER_ERROR, $response->code);
        $this->assertEquals('Message could not be sent: Exception - Test', $response->message);
    }

    public function testSendResponseIsBadRequest()
    {
        $mailService = $this->createMock(MandrillMailService::class);
        $mailService
            ->method('send')
            ->willReturn(false);

        $messageService = $this->createMessageServiceMock(null, $mailService);

        $response = $messageService->send($this->message);

        $this->assertEquals(StatusCode::BAD_REQUEST, $response->code);
        $this->assertEquals('Message could not be sent', $response->message);
    }

    public function testSaveResponseIsCreated()
    {
        $message = clone $this->message;
        $message->setId(self::MESSAGE_ID);

        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('save')
            ->willReturn($message);

        $messageService = $this->createMessageServiceMock($messageModel);

        $response = $messageService->save($this->message);

        $this->assertEquals(StatusCode::CREATED, $response->code);
        $this->assertEquals('Message was created successfully', $response->message);
        $this->assertEquals($message->getId(), $response->data->messageId);
    }

    public function testSaveResponseIsInternalServerError()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('save')
            ->will($this->throwException(new \Exception('Test')));

        $messageService = $this->createMessageServiceMock($messageModel);

        $response = $messageService->save($this->message);

        $this->assertEquals(StatusCode::INTERNAL_SERVER_ERROR, $response->code);
        $this->assertEquals('Message could not be created: Exception - Test', $response->message);
    }

    public function testUploadAttachmentResponseIsOk()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('exists')
            ->willReturn(true);
        $messageModel
            ->method('uploadAttachment')
            ->willReturn(true);

        $messageService = $this->createMessageServiceMock($messageModel);

        $response = $messageService->uploadAttachment(self::MESSAGE_ID, $this->attachment);

        $this->assertEquals(StatusCode::OK, $response->code);
        $this->assertEquals('Attachment was uploaded successfully', $response->message);
    }

    public function testUploadAttachmentResponseIsBadRequest()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('exists')
            ->willReturn(true);
        $messageModel
            ->method('uploadAttachment')
            ->willReturn(false);

        $messageService = $this->createMessageServiceMock($messageModel);

        $response = $messageService->uploadAttachment(self::MESSAGE_ID, $this->attachment);

        $this->assertEquals(StatusCode::BAD_REQUEST, $response->code);
        $this->assertEquals('Attachment could not be uploaded', $response->message);
    }

    public function testUploadAttachmentResponseIsInternalServerError()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('exists')
            ->will($this->throwException(new \Exception('Test')));

        $messageService = $this->createMessageServiceMock($messageModel);

        $response = $messageService->uploadAttachment(self::MESSAGE_ID, $this->attachment);

        $this->assertEquals(StatusCode::INTERNAL_SERVER_ERROR, $response->code);
        $this->assertEquals('Upload attachment failed: Exception - Test', $response->message);
    }

    public function testUploadAttachmentResponseIsNotFound()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('exists')
            ->willReturn(false);

        $messageService = $this->createMessageServiceMock($messageModel);

        $response = $messageService->uploadAttachment(self::MESSAGE_ID, $this->attachment);

        $this->assertEquals(StatusCode::NOT_FOUND, $response->code);
        $this->assertEquals('Message not found', $response->message);
    }

    public function testSendByIdResponseIsOk()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('findById')
            ->willReturn($this->message);

        $mailService = $this->createMock(MandrillMailService::class);
        $mailService
            ->method('send')
            ->willReturn(true);

        $messageService = $this->createMessageServiceMock($messageModel, $mailService);

        $response = $messageService->sendById(self::MESSAGE_ID);

        $this->assertEquals(StatusCode::OK, $response->code);
        $this->assertEquals('Message was sent successfully', $response->message);
    }

    public function testSendByIdResponseIsBadRequest()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('findById')
            ->willReturn($this->message);

        $mailService = $this->createMock(MandrillMailService::class);
        $mailService
            ->method('send')
            ->willReturn(false);

        $messageService = $this->createMessageServiceMock($messageModel, $mailService);

        $response = $messageService->sendById(self::MESSAGE_ID);

        $this->assertEquals(StatusCode::BAD_REQUEST, $response->code);
        $this->assertEquals('Message could not be sent', $response->message);
    }

    public function testSendByIdResponseIsInternalServerError()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('findById')
            ->willReturn($this->message);

        $mailService = $this->createMock(MandrillMailService::class);
        $mailService
            ->method('send')
            ->will($this->throwException(new \Exception('Test')));

        $messageService = $this->createMessageServiceMock($messageModel, $mailService);

        $response = $messageService->sendById(self::MESSAGE_ID);

        $this->assertEquals(StatusCode::INTERNAL_SERVER_ERROR, $response->code);
        $this->assertEquals('Message could not be sent: Exception - Test', $response->message);
    }

    public function testSendByIdResponseIsNotFound()
    {
        $messageModel = $this->createMock(MysqlMessageModel::class);
        $messageModel
            ->method('findById')
            ->willReturn(false);

        $messageService = $this->createMessageServiceMock($messageModel);

        $response = $messageService->sendById(self::MESSAGE_ID);

        $this->assertEquals(StatusCode::NOT_FOUND, $response->code);
        $this->assertEquals('Message not found', $response->message);
    }

    protected function createMessageServiceMock($messageModelMock = null, $mailServiceMock = null)
    {
        return new MessageService(
            $this->validator,
            !is_null($messageModelMock) ? $messageModelMock : $this->messageModel,
            !is_null($mailServiceMock) ? $mailServiceMock : $this->mailService,
            $this->view,
            $this->logger
        );
    }
}
