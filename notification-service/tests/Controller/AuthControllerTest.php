<?php

namespace tests\Controller;

use Deporvillage\NotificationService\Controller\AuthController;
use Deporvillage\NotificationService\Service\Auth\AuthService;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Teapot\StatusCode;

class AuthControllerTest extends TestCase
{
    /** @var AuthController */
    protected $controller;

    protected function setUp()
    {
        $authService = new AuthService(
            [
                'secret' => 'secretTest',
                'tokenAuthUser' => 'test@deporvillage.com',
                'tokenAuthPass' => 'passtest',
                'tokenExpirationInterval' => '+1 day',
            ]
        );

        $logger = $this->createMock(Logger::class);

        $this->controller = new AuthController($authService, $logger);
    }

    public function testSetTokenCreated()
    {
        $request = $this->createRequest('test@deporvillage.com', 'passtest');

        $response = new Response();
        $result = $this->controller->setToken($request, $response);

        $this->assertEquals(StatusCode::CREATED, $result->getStatusCode());
    }

    public function testSetTokenBadRequest()
    {
        $this->expectExceptionCode(StatusCode::BAD_REQUEST);

        $request = $this->createRequest('wrongemail@deporvillage.com', 'passtest');

        $response = new Response();
        $this->controller->setToken($request, $response);
    }

    private function createRequest($email, $password)
    {
        $environment = Environment::mock();
        $request = Request::createFromEnvironment($environment);

        return $request->withParsedBody(['email' => $email, 'password' => $password]);
    }
}
