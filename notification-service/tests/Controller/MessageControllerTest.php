<?php

namespace tests\Controller;

use Deporvillage\NotificationService\Controller\MessageController;
use Deporvillage\NotificationService\Entity\Message\MessageBuilder;
use Deporvillage\NotificationService\Service\Message\MessageService;
use Deporvillage\NotificationService\Service\Response as ServiceResponse;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;
use Teapot\StatusCode;

class MessageControllerTest extends TestCase
{
    const MESSAGE_ID = 1;

    /** @var MessageController */
    protected $controller;
    /** @var MessageBuilder */
    protected $messageBuilder;
    /** @var Logger */
    protected $logger;
    /** @var Response */
    protected $response;

    protected function setUp()
    {
        $this->logger = $this->createMock(Logger::class);
        $config = require __DIR__ . '/../../config/messages.php';
        $this->messageBuilder = new MessageBuilder($config);
        $this->response = new Response();
    }

    public function testGenericOk()
    {
        $messageService = $this->createMessageServiceWithSendOk();

        $request = $this->createGenericRequest();
        $result = $this->controllerFactory($messageService)->generic($request, $this->response);

        $this->assertEquals(StatusCode::OK, $result->getStatusCode());
    }

    public function testNewAccountOk()
    {
        $messageService = $this->createMessageServiceWithSendOk();

        $request = $this->createSpecificRequest();
        $result = $this->controllerFactory($messageService)->newAccount($request, $this->response);

        $this->assertEquals(StatusCode::OK, $result->getStatusCode());
    }

    public function testOrderConfirmationOk()
    {
        $messageService = $this->createMessageServiceWithSendOk();

        $request = $this->createSpecificRequest();
        $result = $this->controllerFactory($messageService)->orderConfirmation($request, $this->response);

        $this->assertEquals(StatusCode::OK, $result->getStatusCode());
    }

    public function testGenericDraftOk()
    {
        $messageService = $this->createMessageServiceWithSaveOk();

        $request = $this->createGenericRequest();
        $result = $this->controllerFactory($messageService)->genericDraft($request, $this->response);

        $this->assertEquals(StatusCode::CREATED, $result->getStatusCode());
    }

    public function testUploadAttachmentOk()
    {
        $messageService = $this->createMessageServiceWithUploadAttachmentOk();

        $request = $this->createUploadAttachmentRequest();
        $args = ['messageId' => self::MESSAGE_ID];
        $result = $this->controllerFactory($messageService)->uploadAttachment($request, $this->response, $args);

        $this->assertEquals(StatusCode::OK, $result->getStatusCode());
    }

    public function testSendOk()
    {
        $messageService = $this->createMessageServiceWithSendByIdOk();

        $request = $this->requestFactory();
        $args = ['messageId' => self::MESSAGE_ID];
        $result = $this->controllerFactory($messageService)->send($request, $this->response, $args);

        $this->assertEquals(StatusCode::OK, $result->getStatusCode());
    }

    /**
     * @return Request
     */
    protected function createGenericRequest()
    {
        return $this->requestFactory()->withParsedBody(
            [
                "templateVars" => ["key" => "value"],
                "from" => ["email" => "no-reply@deporvillage.com", "name" => "Deporvillage.com"],
                "to" => [["email" => "no-reply@deporvillage.com", "name" => "Deporvillage.com"]],
                "cc" => [["email" => "no-reply@deporvillage.com", "name" => "Deporvillage.com"]],
                "bcc" => [["email" => "no-reply@deporvillage.com", "name" => "Deporvillage.com"]],
                "replyTo" => ["email" => "no-reply@deporvillage.com", "name" => "Deporvillage.com"],
                "subject" => "Welcome to deporvillage.com"
            ]
        );
    }

    /**
     * @return Request
     */
    protected function createSpecificRequest()
    {
        return $this->requestFactory()
            ->withParsedBody(
                [
                    "templateVars" => ["key" => "value"],
                    "to" => [["email" => "no-reply@deporvillage.com", "name" => "Deporvillage.com"]],
                ]
            )->withHeader('Accept-Language', 'es-ES');
    }

    /**
     * @return Request
     */
    protected function createUploadAttachmentRequest()
    {
        $files = ['attachment' => new UploadedFile(__FILE__)];

        return $this->requestFactory()->withUploadedFiles($files);
    }

    /**
     * @param array $userData
     * @return Request
     */
    protected function requestFactory($userData = [])
    {
        $environment = Environment::mock($userData);

        return Request::createFromEnvironment($environment);
    }

    /**
     * @param MessageService $messageService
     * @return MessageController
     */
    protected function controllerFactory(MessageService $messageService)
    {
        return new MessageController($messageService, $this->messageBuilder, $this->logger);
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function createMessageServiceWithSendOk()
    {
        $messageService = $this->createMock(MessageService::class);
        $serviceResponse = new ServiceResponse(
            StatusCode::OK, 'Message was sent successfully'
        );
        $messageService
            ->method('send')
            ->willReturn($serviceResponse);

        return $messageService;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function createMessageServiceWithSaveOk()
    {
        $messageService = $this->createMock(MessageService::class);
        $serviceResponse = new ServiceResponse(
            StatusCode::CREATED, 'Message was created successfully', ['messageId' => self::MESSAGE_ID]
        );
        $messageService
            ->method('save')
            ->willReturn($serviceResponse);

        return $messageService;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function createMessageServiceWithUploadAttachmentOk()
    {
        $messageService = $this->createMock(MessageService::class);
        $serviceResponse = new ServiceResponse(
            StatusCode::OK, 'Attachment was uploaded successfully'
        );
        $messageService
            ->method('uploadAttachment')
            ->willReturn($serviceResponse);

        return $messageService;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function createMessageServiceWithSendByIdOk()
    {
        $messageService = $this->createMock(MessageService::class);
        $serviceResponse = new ServiceResponse(
            StatusCode::OK, 'Message was sent successfully'
        );
        $messageService
            ->method('sendById')
            ->willReturn($serviceResponse);

        return $messageService;
    }
}
