<?php

namespace Tests\Functional;

use Teapot\StatusCode;

class HomepageTest extends BaseTestCase
{
    /**
     * Test that the index route returns a rendered response containing the text 'Swagger UI'
     */
    public function testGetHomepageWithSwaggerSpecs()
    {
        $response = $this->runApp('GET', '/');

        $this->assertEquals(StatusCode::OK, $response->getStatusCode());
        $this->assertContains('Swagger UI', (string)$response->getBody());
    }

    /**
     * Test that the index route won't accept a post request
     */
    public function testPostHomepageNotAllowed()
    {
        $response = $this->runApp('POST', '/', ['test']);

        $this->assertEquals(StatusCode::METHOD_NOT_ALLOWED, $response->getStatusCode());
        $this->assertContains('Method not allowed', (string)$response->getBody());
    }

    public function testGetSwaggerJson()
    {
        $response = $this->runApp('GET', '/v1/swagger-json');

        $this->assertEquals(StatusCode::CREATED, $response->getStatusCode());
        $this->assertContains('title', (string)$response->getBody());
    }
}