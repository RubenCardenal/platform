# Deporvillage Mailing API

API to create and send transactional emails from Deporvillage (using [Mandrill](https://www.mandrill.com/) API integration to deliver emails)

## Install the Application

Download this repo in your local folder (preferebly in `~/www/deporvillage/api-mailing` folder) and run:

    composer install

## Install Vagrant
To run the application in development, follow the instructions here:

[Vagrant LEMP for Deporvillage Mailing API project](https://bitbucket.org/deporvillage/api-mailing-vagrant)

## Check if API is up and working
If everything went well, go to [Deporvillage Mailing API](http://api-mailing.deporvillage.dev) and you will see API doc.

## Tests
Run this command to run the test suite

	composer test

or this command to run the test suite with coverage

    composer coverage

## Resources

* [Slim Framework v3](https://www.slimframework.com/docs/)
* [Swagger Framework](https://swagger.io/docs/)
* [Swagger-php](https://github.com/zircote/swagger-php/blob/master/README.md)
* [Swagger UI](https://github.com/swagger-api/swagger-ui/blob/master/README.md)
* [JSON Web Token](https://jwt.io/introduction/#)
* [Slim JWT Authentication Middleware](https://github.com/tuupola/slim-jwt-auth/blob/2.x/README.md)
* [Mandrill API Documentation](https://mandrillapp.com/api/docs/index.php.html)
* [Twig](https://twig.symfony.com/doc/2.x/)
* [Monolog - Logging for PHP](https://github.com/Seldaek/monolog/blob/master/README.md)
* [PHPUnit](https://phpunit.de/manual/current/en/index.html)
