<?php
// Messages settings
return [
    'defaultFrom' => [
        'email' => 'no-reply@deporvillage.com',
        'name' => 'Deporvillage.com'
    ],
    // Message types
    'default' => [
        'class' => 'DefaultMessage',
        'templateFile' => 'default.html.twig',
        'fields' => [
            'required' => ['from', 'to', 'subject', 'templateVars'],
            'optional' => ['cc', 'bcc', 'replyTo']
        ]
    ],
    'newAccount' => [
        'class' => 'NewAccountMessage',
        'templateFile' => 'new_account.html.twig',
        'templateSubjects' => [
            'es-ES' => '{{customer.name}} bienvenido/a a deporvillage.com',
            'fr-FR' => '{{customer.name}}, bienvenue sur deporvillage.fr',
            'it-IF' => 'Benvenuto {{customer.name}}',
            'pt-PT' => '{{customer.name}} bem-vindo(a) a deporvillage.pt'
        ],
        'fields' => [
            'required' => ['to', 'templateVars']
        ],
        'from' => [
            'email' => 'atencionalcliente@deporvillage.com',
            'name' => 'Deporvillage.com'
        ]
    ],
    'orderConfirmation' => [
        'class' => 'OrderConfirmationMessage',
        'templateFile' => 'order_confirmation.html.twig',
        'templateSubjects' => [
            'es-ES' => 'Confirmación de pedido {{incrementId}}',
            'fr-FR' => 'Confirmation de votre commande n° {{incrementId}} sur deporvillage',
            'it-IF' => 'Ti ringraziamo per aver acquistato su deporvillage: Nuovo ordine {{incrementId}}',
            'pt-PT' => 'Confirmação de encomenda {{incrementId}}',
        ],
        'fields' => [
            'required' => ['to', 'templateVars']
        ],
    ]
];