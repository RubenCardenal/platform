<?php
return [
    'settings' => [
        'displayErrorDetails' => false,
        'addContentLengthHeader' => false,
        'debug' => false,
        // DB settings
        'db' => [
            'host' => 'localhost',
            'dbname' => 'dpv_api_mailing',
            'user' => 'root',
            'pass' => 'root',
        ],
        // Twig settings
        'view' => [
            'template_path' => __DIR__ . '/../templates/',
            'template_settings' => [
                'cache' => true,
                'debug' => false,
            ]
        ],
        // Monolog settings
        'logger' => [
            'name' => 'dpv-api-mailing',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        // Mandrill settings
        'mandrill' => [
            'apikey' => 'eQohwkl8ROSrH1BOEn_Qag',
        ],
        // JwtAuthentication settings
        'authentication' => [
            'path' => '/v1',
            'passthrough' => ['/v1/swagger-json', '/v1/auth'],
            'header' => 'X-Token',
            'regexp' => '/(.*)/',
            'secret' => 'c3VwZXJzZWNyZXRrZXl5b3VzaG91bGRub3Rjb21taXR0b2dpdGh1Yg==',
            'attribute' => false,
            'secure' => true,
            'tokenAuthUser' => 'it@deporvillage.com',
            'tokenAuthPass' => 'passprod',
            'tokenExpirationInterval' => '+1 day',
        ],
        // Messages settings
        'messages' => require 'messages.php',
    ],
];
