<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
        'debug' => true,
        // DB settings
        'db' => [
            'host' => 'localhost',
            'dbname' => 'dpv_api_mailing',
            'user' => 'root',
            'pass' => 'root',
        ],
        'mongodb' => [
            'host' => 'mongo', //Docker container reference in docker-compose
            'port' => '27017',
            'user' => 'root',
            'pass' => 'example'
        ],
        // Twig settings
        'view' => [
            'template_path' => __DIR__ . '/../templates/',
            'template_settings' => [
                'cache' => false,
                'debug' => true,
            ]
        ],
        // Monolog settings
        'logger' => [
            'name' => 'dpv-api-mailing',
            'path' => __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
        ],
        // Mandrill settings
        'mandrill' => [
            'apikey' => 'c9mfitFJ5PLrgXAxbCMfxw',
        ],
        // JwtAuthentication settings
        'authentication' => [
            'path' => '/v1',
            'passthrough' => ['/v1/swagger-json', '/v1/auth'],
            'header' => 'X-Token',
            'regexp' => '/(.*)/',
            'secret' => 'c3VwZXJzZWNyZXRrZXl5b3VzaG91bGRub3Rjb21taXR0b2dpdGh1Yg==',
            'attribute' => false,
            'secure' => true,
            'relaxed' => [API_HOST],
            'tokenAuthUser' => 'it@deporvillage.com',
            'tokenAuthPass' => 'passdev',
            'tokenExpirationInterval' => '+1 day',
        ],
        // Messages settings
        'messages' => require 'messages.php',
    ],
];
