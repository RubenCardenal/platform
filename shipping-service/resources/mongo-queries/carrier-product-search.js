db.getCollection('shipping_methods').find({
    "country": "ES",
    "allowed_postcodes": { $in : [null, "08026"] },
    "restricted_postcodes": { $nin: ["08026"] },
    "allowed_categories": { $in : [null, 124] },
    "restricted_categories": { $nin: [124] },
    "start_time": { $lte: 1345 },
    "end_time": { $gte: 1345}, //Order placed at 13:45
    "enabled": 1
})