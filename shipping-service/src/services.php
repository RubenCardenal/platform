<?php
use Monolog\Logger;
use Deporvillage\ShippingService\Service\AvailabilityService;
use Deporvillage\ShippingService\Service\ShipmentService;
use Deporvillage\ShippingService\Service\ShipmentUpdateService;
use Deporvillage\ShippingService\Service\ConfigLoaderService;
use Deporvillage\ShippingService\Service\BeanstalkQueueService;
use Deporvillage\ShippingService\Controller\ShippingMethodController;
use Deporvillage\ShippingService\Controller\ShipmentController;
use Deporvillage\ShippingService\Repository\ShippingMethodRepository;
use Deporvillage\ShippingService\Repository\ShipmentRepository;
use Deporvillage\ShippingService\Repository\ConfigFileRepository;
use Deporvillage\ShippingService\Command\ConfigLoaderCommand;
use Deporvillage\ShippingService\Command\ShipmentUpdateCommand;
use Symfony\Component\Console\Application;

$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $uri = "mongodb://" . $db['user'] . ':' . $db['password'] . '@' . $db['hostname'] . ':' . $db['port'];
    $client = new MongoDB\Client($uri);
    return $client->selectDatabase($db['dbname']);
};

$container[Logger::class] = function ($c) {
    $logger = new \Monolog\Logger($c['settings']['logger']['name']);
    $formatter = new \Monolog\Formatter\LineFormatter(null, null, false, true);

    $logger->pushProcessor(new \Monolog\Processor\UidProcessor());

    $stdoutHandler = new \Monolog\Handler\StreamHandler(
        'php://stdout',
        $c['settings']['logger']['level']
    );
    $stdoutHandler->setFormatter($formatter);
    $logger->pushHandler($stdoutHandler);

    $logfileHandler = new \Monolog\Handler\StreamHandler(
        $c['settings']['logger']['path'],
        $c['settings']['logger']['level']
    );
    $logfileHandler->setFormatter($formatter);
    $logger->pushHandler($logfileHandler);

    return $logger;
};

/** Repositories **/
$container[ShippingMethodRepository::class] = function ($c) {
    return new ShippingMethodRepository($c['db']);
};

$container[ConfigFileRepository::class] = function ($c) {
    return new ConfigFileRepository($c['settings']['config_files_path']);
};

/** Services */
$container[BeanstalkQueueService::class] = function ($c) {
    return new BeanstalkQueueService(
        $c['settings']['beanstalk']['host'],
        $c['settings']['beanstalk']['port'],
        $c['settings']['beanstalk']['queueName']
    );
};

$container[ShipmentRepository::class] = function ($c) {
    return new ShipmentRepository($c['db']);
};

$container[AvailabilityService::class] = function ($c) {
    return new AvailabilityService($c[ShippingMethodRepository::class]);
};

$container[ShipmentService::class] = function ($c) {
    return new ShipmentService(
        $c[ShipmentRepository::class],
        $c[Logger::class]
    );
};

$container[ShipmentUpdateService::class] = function ($c) {
    return new ShipmentUpdateService(
        $c[ShipmentRepository::class],
        $c[BeanstalkQueueService::class],
        $c[Logger::class]
    );
};

$container[ConfigLoaderService::class] = function ($c) {
    return new ConfigLoaderService($c[ConfigFileRepository::class], $c[ShippingMethodRepository::class]);
};

/** Controllers */
$container[ShipmentController::class] = function ($c) {
    return new ShipmentController($c[ShipmentService::class], $c[Logger::class]);
};

$container[ShippingMethodController::class] = function ($c) {
    return new ShippingMethodController($c[AvailabilityService::class], $c[Logger::class]);
};

/** Commands **/
$container[ConfigLoaderCommand::class] = function ($c) {
    return new ConfigLoaderCommand(
        $c[ConfigLoaderService::class],
        $c[Logger::class]
    );
};

$container[ShipmentUpdateCommand::class] = function ($c) {
    return new ShipmentUpdateCommand(
        $c[ShipmentUpdateService::class],
        $c[Logger::class]
    );
};

$container['console_app'] = function ($c) {
    $application = new Application(
        $c['settings']['app_name'],
        $c['settings']['app_version']
    );
    $application->addCommands(
        [
            $c[ConfigLoaderCommand::class],
            $c[ShipmentUpdateCommand::class]
        ]
    );

    return $application;
};

