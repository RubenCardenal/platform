<?php

namespace Deporvillage\ShippingService\Builder;

use Deporvillage\ShippingService\Model\ShipmentStatusCodes;

class StatusBuilder
{
    public static function buildStatus($statusCode, $carrierStatusCode = null)
    {
        if (!ShipmentStatusCodes::isValid($statusCode)) {
            return new \Exception("Invalid Status Code provided - " . $statusCode);
        }

        $status = new \stdClass();
        $status->code = $statusCode;
        $status->carrier_status_code = $carrierStatusCode;
        $status->updated_at = new \DateTime();

        return $status;
    }
}