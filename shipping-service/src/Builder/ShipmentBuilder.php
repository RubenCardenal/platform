<?php

namespace Deporvillage\ShippingService\Builder;

class ShipmentBuilder
{
    public static function buildShipment(
        $orderId,
        $shipmentId,
        \stdClass $currentStatus
    ) {
        $shipment = new \stdClass();

        $shipment->order_id = $orderId;
        $shipment->shipment_id = $shipmentId;
        $shipment->tracking_code = null;
        $shipment->carrier = null;
        $shipment->current_status = $currentStatus;
        $shipment->statuses[] = $currentStatus;
        $shipment->created_at = new \DateTime();

        return $shipment;
    }
}