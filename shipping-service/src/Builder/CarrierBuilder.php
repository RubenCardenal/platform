<?php

namespace Deporvillage\ShippingService\Builder;

class CarrierBuilder
{
    public static function buildCarrier($code)
    {
        $carrier = new \stdClass();
        $carrier->code = $code;

        return $carrier;
    }
}