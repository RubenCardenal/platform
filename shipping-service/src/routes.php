<?php

use Deporvillage\ShippingService\Controller\ShippingMethodController;
use Deporvillage\ShippingService\Controller\ShipmentController;

//Shipping Methods
$app->get('/shipping-methods', ShippingMethodController::class . ':find');

//Shipments
$app->get('/order-shipments/{orderId}', ShipmentController::class. ':getOrderShipments');
