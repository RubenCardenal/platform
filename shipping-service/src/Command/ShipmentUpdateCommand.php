<?php

namespace Deporvillage\ShippingService\Command;

use Deporvillage\ShippingService\Service\ShipmentUpdateService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ShipmentUpdateCommand extends Command
{
    /** @var ShipmentService */
    protected $service;

    /** @var LoggerInterface */
    protected $logger;

    protected function configure()
    {
        $this->setName('shipment:update')->setDescription(
            'Read shipment notifications from queue and create/update shipments'
        );
    }

    public function __construct(
        ShipmentUpdateService $service,
        LoggerInterface $logger
    ) {
        $this->service = $service;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->logger->info('Begin command ' . $this->getName());
            $this->service->execute();
            $this->logger->info('End command ' . $this->getName());
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
