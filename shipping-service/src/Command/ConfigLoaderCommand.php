<?php

namespace Deporvillage\ShippingService\Command;

use Deporvillage\ShippingService\Service\ConfigLoaderService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ConfigLoaderCommand extends Command
{
    /** @var ConfigLoaderService */
    protected $service;

    /** @var LoggerInterface */
    protected $logger;

    protected function configure()
    {
        $this
            ->setName('shipping-method:config:load')
            ->setDescription('Load shipping methods config files into database');
    }

    public function __construct(
        ConfigLoaderService $service,
        LoggerInterface $logger
    ) {
        $this->service = $service;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->logger->info('Begin command ' . $this->getName());
            $insertedCount = $this->service->execute();
            $this->logger->info('Configurations loaded: ' . $insertedCount);
            $this->logger->info('End command ' . $this->getName());
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
