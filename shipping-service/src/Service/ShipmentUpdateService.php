<?php

namespace Deporvillage\ShippingService\Service;

use Deporvillage\ShippingService\Builder\CarrierBuilder;
use Deporvillage\ShippingService\Builder\ShipmentBuilder;
use Deporvillage\ShippingService\Builder\StatusBuilder;
use Deporvillage\ShippingService\Model\ShipmentAction;
use Deporvillage\ShippingService\Model\ShipmentSearchCriteria;
use Deporvillage\ShippingService\Model\ShipmentStatusCodes;
use Deporvillage\ShippingService\Model\ShipmentUpdateData;
use Deporvillage\ShippingService\Repository\ShipmentRepositoryInterface;
use Psr\Log\LoggerInterface;

class ShipmentUpdateService
{
    /** @var ShipmentRepositoryInterface */
    protected $repository;
    /** @var QueueService */
    protected $queue;
    /** @var LoggerInterface */
    protected $logger;
    /** @var int */
    protected $startTime;
    /** @var int */
    protected $maxRunTime;

    public function __construct(
        ShipmentRepositoryInterface $repository,
        QueueService $queue,
        LoggerInterface $logger,
        $maxRuntime = 3600
    ) {
        $this->repository = $repository;
        $this->queue = $queue;
        $this->logger = $logger;
        $this->startTime = time();
        $this->maxRunTime = $maxRuntime;
    }

    public function execute()
    {
        $this->logger->info('Started shipment update #' . getmypid());

        while (($ttl = $this->maxRunTime - $this->getRunTime()) > 0) {
            try {
                if ($job = $this->queue->pop($ttl)) {
                    $this->logger->info('Processing job ' . json_encode($job));
                    $this->processJob($job);
                    $this->queue->clear();
                    $this->logger->info(
                        'Shipment #' . $job['shipment_id'] . ': Action ' . $job['action'] . ' executed successfully'
                    );
                }
            } catch (\Exception $e) {
                if (!empty($job)) {
                    $this->queue->bury();
                    $this->logger->error(
                        'Shipment #' . $job['shipment_id'] . ': Action ' . $job['action'] . ' failed: ' . $e->getMessage()
                    );
                } else {
                    $this->logger->error($e->getMessage());
                }
            }
        };

        $this->logger->info('Max run time reached for shipment update #' . getmypid());
    }

    /**
     * @param array $job
     * @return mixed
     * @throws \Exception
     */
    protected function processJob($job)
    {
        if (!ShipmentAction::isValid($job['action'])) {
            throw new \Exception('Invalid action');
        }

        switch ($job['action']) {
            case ShipmentAction::CREATE:
                $this->createShipment(
                    $job['order_id'],
                    $job['shipment_id']
                );
                break;
            case ShipmentAction::UPDATE:
                $this->updateShipment(
                    $job['order_id'],
                    $job['shipment_id'],
                    $job['carrier_code'],
                    $job['tracking_code'],
                    $job['status_code'],
                    $job['carrier_status_code']
                );
                break;
        }

        return true;
    }

    /**
     * @param string $orderId
     * @param string $shipmentId
     * @return bool
     * @throws \Exception
     */
    protected function createShipment($orderId, $shipmentId)
    {
        $criteria = new ShipmentSearchCriteria();
        $criteria->orderId = $orderId;
        $criteria->shipmentId = $shipmentId;
        $shipment = $this->repository->find($criteria);
        if (!is_null($shipment)) {
            throw new \Exception('Shipment already exists');
        }

        $status = StatusBuilder::buildStatus(ShipmentStatusCodes::BOOKED);
        $shipment = ShipmentBuilder::buildShipment($orderId, $shipmentId, $status);

        $this->repository->add($shipment);

        return true;
    }

    /**
     * @param string $orderId
     * @param string $shipmentId
     * @param string $carrierCode
     * @param string $trackingCode
     * @param string $statusCode
     * @param string $carrierStatusCode
     * @return bool
     * @throws \Exception
     */
    protected function updateShipment($orderId, $shipmentId, $carrierCode, $trackingCode, $statusCode, $carrierStatusCode)
    {
        /*
         * TODO: Currently shipment updates doesn't take into account if the status has already been imported or not
         * We'll need a mechanism to validate already imported status etc to avoid duplicate status registries.
         */

        $criteria = new ShipmentSearchCriteria();
        $criteria->orderId = $orderId;
        $criteria->shipmentId = $shipmentId;

        $shipmentUpdateData = new ShipmentUpdateData();
        $shipmentUpdateData->carrier = CarrierBuilder::buildCarrier($carrierCode);
        $shipmentUpdateData->trackingCode = $trackingCode;
        $shipmentUpdateData->status = StatusBuilder::buildStatus($statusCode, $carrierStatusCode);

        $result = $this->repository->update($criteria, $shipmentUpdateData);

        if (is_null($result)) {
            throw new \Exception('Shipment not found');
        }

        return true;
    }

    /**
     * @return int
     */
    protected function getRunTime()
    {
        return time() - $this->startTime;
    }
}