<?php

namespace Deporvillage\ShippingService\Service;

use Deporvillage\ShippingService\Repository\ShipmentRepositoryInterface;
use Psr\Log\LoggerInterface;

class ShipmentService
{
    /** @var ShipmentRepositoryInterface */
    protected $repository;
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(
        ShipmentRepositoryInterface $repository,
        LoggerInterface $logger
    ) {
        $this->repository = $repository;
        $this->logger = $logger;
    }

    /**
     * Returns an array of all the shipments for a given OrderId
     *
     * @param string $orderId
     * @return array
     */
    public function getOrderShipments($orderId)
    {
        return $this->repository->findByOrderId($orderId)->toArray();
    }
}