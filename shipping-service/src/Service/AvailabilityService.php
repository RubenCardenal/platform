<?php

namespace Deporvillage\ShippingService\Service;

use Deporvillage\ShippingService\Repository\Criteria;
use Deporvillage\ShippingService\Repository\ShippingMethodRepositoryInterface;

class AvailabilityService
{
    /** @var ShippingMethodRepositoryInterface */
    private $repository;

    public function __construct(ShippingMethodRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Criteria $criteria
     * @return array
     */
    public function execute(Criteria $criteria)
    {
        $shippingMethods = $this->repository->find($criteria);
        foreach ($shippingMethods as $key => $shippingMethod) {
            try {
                $shippingMethod['price'] = $this->calculateShippingPrice($criteria, $shippingMethod['price']);
            } catch (\Exception $e) {
                unset($shippingMethods[$key]);
            }
        }

        return $shippingMethods;
    }

    /**
     * @param Criteria $criteria
     * @param array $price
     * @return float|null
     * @throws \Exception
     */
    protected function calculateShippingPrice(Criteria $criteria, $price)
    {
        $shippingPrice = null;

        if (!empty($price['postcode_pricing'])) {
            $shippingPrice = $this->calculateShippingPriceByPostcode(
                $price['postcode_pricing'],
                $criteria->categories,
                $criteria->postcode
            );
        }

        if (is_null($shippingPrice) && !empty($price['category_pricing'])) {
            $shippingPrice = $this->calculateShippingPriceByCategories(
                $price['category_pricing'],
                $criteria->categories
            );
        }

        if (is_null($shippingPrice) && !empty($price['cart_pricing'])) {
            $shippingPrice = $this->calculateShippingPriceByCartAmount(
                $price['cart_pricing'],
                $criteria->cartAmount
            );
        }

        return $shippingPrice;
    }

    /**
     * @param array $prices
     * @param array $categories
     * @param string $postcode
     * @return null|float
     * @throws \Exception
     */
    protected function calculateShippingPriceByPostcode($prices, $categories, $postcode)
    {
        foreach ($prices as $price) {
            if (in_array($postcode, (array)($price->allowed_postcodes))) {
                if (!empty(array_intersect((array)($price->restricted_categories), $categories))) {
                    throw new \Exception('Shipping price not available (Restricted categories)');
                }
                return $price->value;
            }
        }

        return null;
    }

    /**
     * @param $prices
     * @param $categories
     * @return float
     * @throws \Exception
     */
    protected function calculateShippingPriceByCategories($prices, $categories)
    {
        foreach ($prices as $price) {
            if (!empty(array_intersect((array)($price->allowed_categories), $categories))) {
                return $price->value;
            }
        }

        return null;
    }

    /**
     * @param $prices
     * @param $cartAmount
     * @return float
     * @throws \Exception
     */
    protected function calculateShippingPriceByCartAmount($prices, $cartAmount)
    {
        $shippingPrice = null;
        $maxMinAmount = 0.0;
        foreach ($prices as $price) {
            if ($cartAmount > $price['min_amount'] && $price['min_amount'] >= $maxMinAmount) {
                $shippingPrice = $price['value'];
                $maxMinAmount = $price['min_amount'];
            }
        }

        if (is_null($shippingPrice)) {
            throw new \Exception('Shipping price not available');
        }

        return $shippingPrice;
    }
}