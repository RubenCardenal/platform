<?php

namespace Deporvillage\ShippingService\Service;

abstract class QueueService
{
    protected $queueName;

    const DEFAULT_QUEUE_NAME = 'default';

    /**
     * Queue constructor.
     * @param string $queueName
     */
    public function __construct($queueName = self::DEFAULT_QUEUE_NAME)
    {
        $this->setQueueName($queueName);
    }

    /**
     * Set queue name
     *
     * @param string $queueName
     */
    public function setQueueName($queueName)
    {
        $this->queueName = $queueName;
    }

    /**
     * Get queue name
     *
     * @return string
     */
    public function getQueueName()
    {
        return $this->queueName;
    }

    /**
     * Pushes the job onto the queue
     *
     * @param $job
     * @return mixed
     */
    abstract public function push($job);

    /**
     * Blocks until a job is available or timeout is reached, and returns it
     *
     * @param int $timeout
     * @return mixed Returns a job when is available or null when timeot
     */
    abstract public function pop($timeout = 0);

    /**
     * Delete current job
     *
     * @return mixed
     */
    abstract public function clear();

    /**
     * Bury current job
     *
     * @return mixed
     */
    abstract public function bury();
}
