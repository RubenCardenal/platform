<?php

namespace Deporvillage\ShippingService\Service;

use Deporvillage\ShippingService\Repository\ConfigFileRepositoryInterface;
use Deporvillage\ShippingService\Repository\ShippingMethodRepositoryInterface;

class ConfigLoaderService
{
    private $configFileRepository;

    /** @var ShippingMethodRepositoryInterface */
    private $shippingMethodRepository;

    public function __construct(
        ConfigFileRepositoryInterface $configFileReRepository,
        ShippingMethodRepositoryInterface $repository
    ) {
        $this->configFileRepository = $configFileReRepository;
        $this->shippingMethodRepository = $repository;
    }

    /**
     * @return int
     */
    public function execute()
    {
        $configFiles = $this->configFileRepository->findAll();
        $documents = $this->shippingMethodRepository->fromJSONToPHP($configFiles);
        $this->shippingMethodRepository->removeAll();

        return $this->shippingMethodRepository->add($documents);
    }
}


