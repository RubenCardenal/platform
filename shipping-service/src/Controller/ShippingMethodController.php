<?php

namespace Deporvillage\ShippingService\Controller;

use Deporvillage\ShippingService\Repository\Criteria;
use Deporvillage\ShippingService\Service\AvailabilityService;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Teapot\StatusCode;

class ShippingMethodController
{
    /** @var AvailabilityService */
    protected $availabilityService;
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(AvailabilityService $availabilityService, LoggerInterface $logger)
    {
        $this->availabilityService = $availabilityService;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param $args
     * @return Response
     */
    public function find(Request $request, Response $response, $args)
    {
        $this->logger->info('Request Uri: ' . $request->getUri());

        $data = [];
        if ($this->isValidRequest($request)) {
            $criteria = new Criteria();
            $criteria->country = $request->getParam('country');
            $criteria->postcode = $request->getParam('postcode', '');
            $criteria->categories = array_map('intval', explode("|", $request->getParam('categories')));
            $criteria->cartAmount = (float)$request->getParam('cart-amount');
            $criteria->hasStock = (bool)$request->getParam('has-stock');
            $criteria->onlyPublic = (bool)$request->getParam('only-public', false);

            try {
                $data = $this->availabilityService->execute($criteria);
                $status = StatusCode::OK;
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
                $status = StatusCode::INTERNAL_SERVER_ERROR;
            }
        } else {
            $status = StatusCode::BAD_REQUEST;
        }

        $response = $response->withJson($data, $status);

        $this->logger->info('Response Status: ' . $response->getStatusCode() . ' ' . $response->getReasonPhrase());
        $this->logger->info('Response Body: ' . (string)$response->getBody());

        return $response;
    }

    /**
     * @param Request $request
     * @return bool
     */
    protected function isValidRequest(Request $request)
    {
        if (empty($request->getParam('country'))) {
            $this->logger->error('Param "country" cannot be empty');
            return false;
        }
        if (empty($request->getParam('categories'))) {
            $this->logger->error('Param "categories" cannot be empty');
            return false;
        }
        if (empty($request->getParam('cart-amount'))) {
            $this->logger->error('Param "cart-amount" cannot be empty');
            return false;
        }

        return true;
    }
}