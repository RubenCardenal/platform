<?php

namespace Deporvillage\ShippingService\Controller;

use Deporvillage\ShippingService\Service\ShipmentService;
use Psr\Log\LoggerInterface;
use Slim\Http\Request;
use Slim\Http\Response;
use Teapot\StatusCode\Http;

class ShipmentController
{
    /** @var ShipmentService */
    protected $shipmentService;
    /** @var LoggerInterface */
    protected $logger;

    public function __construct(ShipmentService $shipmentService, LoggerInterface $logger)
    {
        $this->shipmentService = $shipmentService;
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     */
    public function getOrderShipments(Request $request, Response $response)
    {
        $orderId = $request->getAttribute('route')->getArgument('orderId');

        /** @var Cursor $shipments */
        $shipments = $this->shipmentService->getOrderShipments($orderId);
        $statusCode =  Http::OK;
        if(empty($shipments)) {
            $statusCode = Http::NOT_FOUND;
        }
        $response = $response->withJson($shipments, $statusCode);

        return $response;
    }
}