<?php

namespace Deporvillage\ShippingService\Model;

class ShipmentSearchCriteria
{
    /** @var string */
    public $orderId;
    /** @var string */
    public $shipmentId;
}