<?php

namespace Deporvillage\ShippingService\Model;

use MyCLabs\Enum\Enum;

class ShipmentAction extends Enum
{
    const CREATE = 'shipment.create';
    const UPDATE = 'shipment.update';
}