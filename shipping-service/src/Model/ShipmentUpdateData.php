<?php

namespace Deporvillage\ShippingService\Model;

class ShipmentUpdateData
{
    /** @var \stdClass */
    public $carrier;

    /** @var string */
    public $trackingCode;

    /** @var \stdClass */
    public $status;
}