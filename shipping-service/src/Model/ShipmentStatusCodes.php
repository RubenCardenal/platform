<?php

namespace Deporvillage\ShippingService\Model;

use MyCLabs\Enum\Enum;

class ShipmentStatusCodes extends Enum
{
    /** @var string When warehouse notifies Carrier of a package */
    const BOOKED = "BOOKED";
    /** @var string When Carrier picks up the package from the warehouse */
    const SHIPPED = "SHIPPED";
    /** @var string In Transit */
    const INTRANSIT = "INTRANSIT";
    /** @var string Last mile delivery status */
    const READYFORDELIVERY = "READYFORDELIVERY";
    /** @var string Delivered  or customer picked up */
    const DELIVERED = "DELIVERED";
    /** @var string Ready for pickup from pick up location */
    const READYFORCUSTOMERPICKUP = "READYFORCUSTOMERPICKUP";
    /** @var string Generic Carrier Exception, should look into Carrier Status Code for more details */
    const EXCEPTION = "EXCEPTION";
}