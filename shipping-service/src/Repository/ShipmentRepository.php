<?php

namespace Deporvillage\ShippingService\Repository;

use Deporvillage\ShippingService\Model\ShipmentSearchCriteria;
use Deporvillage\ShippingService\Model\ShipmentUpdateData;
use \MongoDB\Database;
use \MongoDB\Collection;
use MongoDB\Operation\FindOneAndUpdate;

class ShipmentRepository implements ShipmentRepositoryInterface
{
    const COLLECTION_NAME = 'shipments';

    /** @var Collection */
    protected $dbCollection;

    /**
     * @param Database $db
     */
    public function __construct(Database $db)
    {
        $this->dbCollection = $db->selectCollection(self::COLLECTION_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function add(\stdClass $shipment)
    {
        $result = $this->dbCollection->insertOne(json_decode(json_encode($shipment)));

        return $this->findById($result->getInsertedId());
    }

    /**
     * {@inheritdoc}
     */
    public function update(ShipmentSearchCriteria $criteria, ShipmentUpdateData $data)
    {
        $filter = [
            "order_id" => $criteria->orderId,
            "shipment_id" => $criteria->shipmentId
        ];

        $update = [];
        if ($data->status !== null) {
            $update['$addToSet']['statuses'] = $data->status;
            $update['$set']['current_status'] = $data->status;
        }

        if ($data->carrier !== null) {
            $update['$set']['carrier'] = $data->carrier;
        }

        if ($data->trackingCode !== null) {
            $update['$set']['tracking_code'] = $data->trackingCode;
        }

        $options = ['returnDocument' => FindOneAndUpdate::RETURN_DOCUMENT_AFTER];

        return $this->dbCollection->findOneAndUpdate($filter, $update, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function find(ShipmentSearchCriteria $criteria)
    {
        return $this->dbCollection->findOne(
            [
                "order_id" => $criteria->orderId,
                "shipment_id" => $criteria->shipmentId
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        return $this->dbCollection->findOne(["_id" => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function findByOrderId($orderId)
    {
        return $this->dbCollection->find(['order_id' => $orderId]);
    }
}