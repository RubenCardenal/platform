<?php

namespace Deporvillage\ShippingService\Repository;

interface ConfigFileRepositoryInterface
{
    /**
     * @return array
     */
    public function findAll();
}