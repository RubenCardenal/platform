<?php

namespace Deporvillage\ShippingService\Repository;

interface ShippingMethodRepositoryInterface
{
    /**
     * @param Criteria $criteria
     * @return array
     */
    public function find(Criteria $criteria);

    /**
     * @return bool
     */
    public function removeAll();

    /**
     * @param array $documents
     * @return int
     */
    public function add(array $documents);

    /**
     * @param array $items
     * @return array
     */
    public function fromJSONToPHP(array $items);
}