<?php

namespace Deporvillage\ShippingService\Repository;

class Criteria
{
    /** @var string */
    public $country;
    /** @var string */
    public $postcode;
    /** @var array */
    public $categories;
    /** @var float */
    public $cartAmount;
    /** @var bool */
    public $onlyPublic;
    /** @var bool */
    public $hasStock;
    /** @var bool */
    public $enabled = true;
}