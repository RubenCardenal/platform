<?php

namespace Deporvillage\ShippingService\Repository;

use Symfony\Component\Finder\Finder;

class ConfigFileRepository implements ConfigFileRepositoryInterface
{
    /** @var string */
    protected $path;

    /**
     * @param string $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll()
    {
        $finder = new Finder();
        $finder->files()->in($this->path)->name('*.bson');

        $contents = [];
        foreach ($finder as $file) {
            $contents[] = $file->getContents();
        }

        return $contents;
    }
}