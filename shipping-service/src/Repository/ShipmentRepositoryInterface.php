<?php

namespace Deporvillage\ShippingService\Repository;

use Deporvillage\ShippingService\Model\ShipmentSearchCriteria;
use Deporvillage\ShippingService\Model\ShipmentUpdateData;

interface ShipmentRepositoryInterface
{
    /**
     * @param \stdClass $shipment
     * @return array|null|object
     */
    public function add(\stdClass $shipment);

    /**
     * @param ShipmentSearchCriteria $criteria
     * @param ShipmentUpdateData $data
     * @return array|null|object
     */
    public function update(ShipmentSearchCriteria $criteria, ShipmentUpdateData $data);

    /**
     * @param ShipmentSearchCriteria $criteria
     * @return array|null|object
     */
    public function find(ShipmentSearchCriteria $criteria);

    /**
     * @param $id
     * @return array|null|object
     */
    public function findById($id);

    /**
     * Finds all the Shipments for an Order
     *
     * @param string $orderId
     * @return \MongoDB\Driver\Cursor
     */
    public function findByOrderId($orderId);
}