<?php

namespace Deporvillage\ShippingService\Repository;

use MongoDB\Database;
use MongoDB\Collection;

class ShippingMethodRepository implements ShippingMethodRepositoryInterface
{
    const COLLECTION_NAME = 'shipping_methods';

    /** @var Collection */
    protected $dbCollection;

    /**
     * @param Database $db
     */
    public function __construct(Database $db)
    {
        $this->dbCollection = $db->selectCollection(self::COLLECTION_NAME);
    }

    /**
     * @param Criteria $criteria
     * @return array
     */
    public function find(Criteria $criteria)
    {
        $filter = $this->prepareFilter($criteria);
        $options = [
            "projection" => [
                "_id" => false,
                "code" => true,
                "title" => true,
                "magento_carrier" => true,
                "carrier" => true,
                "price" => true,
                "cost" => true,
                "logistics_code" => true
            ]
        ];

        return iterator_to_array($this->dbCollection->find($filter, $options));
    }

    /**
     * @return bool
     */
    public function removeAll()
    {
        $result = $this->dbCollection->drop();

        return (bool)$result->ok;
    }

    /**
     * @param array $documents
     * @return int
     */
    public function add(array $documents)
    {
        return $this->dbCollection->insertMany($documents)->getInsertedCount();
    }

    /**
     * @param array $items
     * @return array
     */
    public function fromJSONToPHP(array $items)
    {
        return array_map('MongoDB\BSON\toPHP', array_map('MongoDB\BSON\fromJSON', $items));
    }

    /**
     * @param Criteria $criteria
     * @return array
     */
    private function prepareFilter(Criteria $criteria)
    {
        $filter['country'] = $criteria->country;

        $filter['allowed_postcodes'] = ['$in' => [null, $criteria->postcode]];
        $filter['restricted_postcodes'] = ['$nin' => [$criteria->postcode]];

        $filter['allowed_categories'] = ['$in' => array_merge([null], $criteria->categories)];
        $filter['restricted_categories'] = ['$nin' => $criteria->categories];

        if ($criteria->onlyPublic) {
            $filter['public'] = 1;
        }

        if (!$criteria->hasStock) {
            $filter['need_stock'] = 0;
        }

        if ($criteria->enabled) {
            $filter['enabled'] = 1;
        }

        $currentTime = (int)date("Hi");
        $filter['start_time'] = ['$lte' => $currentTime];
        $filter['end_time'] = ['$gte' => $currentTime];

        return $filter;
    }
}