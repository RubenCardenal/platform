FROM composer:1.7.2 AS composer

# Use an official PHP 7.2 image
FROM php:7.2-apache

MAINTAINER Pavel Pratyush

RUN apt-get update
RUN apt-get install -y zip unzip libzip-dev libxml2-dev \
    && docker-php-ext-configure zip --with-libzip \
    && docker-php-ext-install zip \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug

# SYS Devel packages
RUN apt-get install -y iputils-ping telnet curl

#The libraries are needed for Mongo supporting SSL
RUN apt-get install -y libcurl4-openssl-dev pkg-config libssl-dev
RUN pecl install mongodb
RUN echo "extension=mongodb.so" >> /usr/local/etc/php/conf.d/mongodb.ini

COPY . /srv
COPY .docker/vhost.conf /etc/apache2/sites-available/000-default.conf
COPY .docker/hosts /etc/hosts

#Copy Composer PHAR from official build
COPY --from=composer /usr/bin/composer /usr/bin/composer
# Optionally if Composer needs to run as root
# ENV COMPOSER_ALLOW_SUPERUSER 1 

RUN chown -R www-data:www-data /srv/public
RUN a2enmod rewrite

# Set the WORKDIR to /app so all following commands run in /app
WORKDIR /srv

# Install dependencies with Composer.
# --prefer-source fixes issues with download limits on GitHub.
# --no-interaction makes sure composer can run fully automated
RUN composer install --no-interaction