<?php
return [
    'app_name' => 'Shipping Service',
    'app_version' => '0.0.1',
    'displayErrorDetails' => true, // set to false in production
    'addContentLengthHeader' => false, // Allow the web server to send the content-length header
    'db' => [
        'hostname' => 'mongo',
        'port' => '27017',
        'dbname' => 'shipping-service',
        'user' => 'root',
        'password' => 'example'
    ],
    'logger' => [
        'name' => 'shipping-service',
        'path' => __DIR__ . '/../../logs/app.log',
        'level' => \Monolog\Logger::DEBUG,
    ],
    'config_files_path' => __DIR__ . '/../../resources/shipping/methods/',
    'beanstalk' => [
        'host' => 'beanstalk-pre.deporvillage.tech',
        'port' => 11300,
        'queueName' => 'shipping-service'
    ],
    'worker' => [
        'max_runtime' => 3600
    ]
];