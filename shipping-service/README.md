# Shipping Service

Microservice to decouple Shipping Options and Events from Magento Webstore

### Installing

Clone the repository, compose docker, run composer go!


First run

```
git clone <this repository>
docker-compose up --build

#Open a new terminal window for the location of the code and execute
docker-compose exec shipping-service composer install
chmod 777 shipping-service/logs
cp shipping-service/src/settings.sample.php shipping-service/src/settings.php
```

Subsequent runs

```
docker-compose up
```

For updating PHP Dependencies
```
docker-compose exec app composer install
```

## Tests
Run this command to run the test suite

	docker-compose exec order-service composer test

or this command to run the test suite with coverage

    docker-compose exec order-service composer coverage
    
### API calls

Get Shipping-methods

Request:
```
http://localhost:8080/shipping-methods?country=FR&postcode=3120&categories=5&has-stock=1&only-public=1&cart-amount=10
```
Response:
```
[
    {
        "code": "GLS",
        "title": "Standard shipping",
        "magento_carrier": {
            "code": "tablerate",
            "method": "bestway"
        },
        "carrier": {
            "code": "gls",
            "name": "gls"
        },
        "price": 10.99,
        "cost": 2,
        "logistics_code": "GLS"
    }
]
```
### Commands

Load shipping methods from config files into database
```
docker-compose exec shipping-service php bin/console shipping-method:config:load
```
Read shipment notifications from queue and create/update shipments
```
docker-compose exec shipping-service php bin/console shipment:update
```