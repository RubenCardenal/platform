<?php

require __DIR__ . '/../app/bootstrap.php';

//Register Routes
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
