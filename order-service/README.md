# Order Service

Microservice to decouple Order Events from Magento Webstore

### Installing

Clone the repository, compose docker, run composer go!


First run

```
git clone <this repository>
docker-compose up --build

#Open a new terminal window for the location of the code and execute
docker-compose exec order-service composer install
chmod 777 order-service/logs
cp order-service/src/settings.sample.php order-service/src/settings.php
```

Subsequent runs

```
docker-compose up
```

For updating PHP Dependencies
```
docker-compose exec app composer install
```

## Tests
Run this command to run the test suite

	docker-compose exec order-service composer test

or this command to run the test suite with coverage

    docker-compose exec order-service composer coverage
    
### Commands

Read new order notifications from queue and import orders from Magento API into database
```
docker-compose exec order-service php bin/console order:import
```