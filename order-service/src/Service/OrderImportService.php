<?php

namespace Deporvillage\OrderService\Service;

use Deporvillage\OrderService\Builder\OrderBuilder;
use Deporvillage\OrderService\Builder\StatusBuilder;
use Deporvillage\OrderService\Model\OrderAction;
use Deporvillage\OrderService\Model\OrderStatusCodes;
use Deporvillage\OrderService\Repository\ApiOrderRepositoryInterface;
use Deporvillage\OrderService\Repository\OrderRepositoryInterface;
use Exception;
use Psr\Log\LoggerInterface;

class OrderImportService
{
    /** @var OrderRepositoryInterface */
    protected $orderRepository;
    /** @var ApiOrderRepositoryInterface */
    protected $apiOrderRepository;
    /** @var QueueService */
    protected $queue;
    /** @var LoggerInterface */
    protected $logger;
    protected $startTime;
    protected $maxRunTime;

    public function __construct(
        OrderRepositoryInterface $orderRepository,
        ApiOrderRepositoryInterface $apiOrderRepository,
        QueueService $queue,
        LoggerInterface $logger,
        $maxRuntime = 3600
    ) {
        $this->orderRepository = $orderRepository;
        $this->apiOrderRepository = $apiOrderRepository;
        $this->queue = $queue;
        $this->logger = $logger;
        $this->startTime = time();
        $this->maxRunTime = $maxRuntime;
    }

    public function execute()
    {
        $this->logger->info('Started order import #' . getmypid());

        while (($ttl = $this->maxRunTime - $this->getRunTime()) > 0) {
            try {
                if ($job = $this->queue->pop($ttl)) {
                    $this->logger->info('Processing job ' . json_encode($job));
                    $this->processJob($job);
                    $this->queue->clear();
                    $this->logger->info(
                        'Order #' . $job['order_id'] . ': Action ' . $job['action'] . ' executed successfully'
                    );
                }
            } catch (Exception $e) {
                if (!empty($job)) {
                    $this->queue->bury();
                    $this->logger->error(
                        'Order #' . $job['order_id'] . ': Action ' . $job['action'] . ' failed: ' . $e->getMessage()
                    );
                } else {
                    $this->logger->error($e->getMessage());
                }
            }
        };

        $this->logger->info('Max run time reached for order import #' . getmypid());
    }

    /**
     * @param array $job
     * @return bool
     * @throws Exception
     */
    protected function processJob($job)
    {
        if (!OrderAction::isValid($job['action'])) {
            throw new Exception('Invalid action');
        }

        switch ($job['action']) {
            case OrderAction::CREATE:
                $this->createOrder($job['order_id']);
                break;
            case OrderAction::CANCEL:
                $this->cancelOrder($job['order_id']);
                break;
        }

        return true;
    }

    /**
     * @param $orderId
     * @return bool
     * @throws Exception
     */
    protected function createOrder($orderId)
    {
        $order = $this->orderRepository->findByOrderId($orderId);
        if (!is_null($order)) {
            throw new Exception('Order already exists');
        }

        $apiOrder = $this->apiOrderRepository->get($orderId);
        $status = StatusBuilder::buildStatus(OrderStatusCodes::CREATED);
        $order = OrderBuilder::buildOrder($orderId, $status, $apiOrder);
        $this->orderRepository->add($order);

        return true;
    }

    /**
     * @param $orderId
     * @return bool
     * @throws Exception
     */
    protected function cancelOrder($orderId)
    {
        $status = StatusBuilder::buildStatus(OrderStatusCodes::CANCELLED);

        $result = $this->orderRepository->updateStatus($orderId, $status);

        if (is_null($result)) {
            throw new Exception('Order not found');
        }

        return true;
    }

    /**
     * @return int
     */
    protected function getRunTime()
    {
        return time() - $this->startTime;
    }
}
