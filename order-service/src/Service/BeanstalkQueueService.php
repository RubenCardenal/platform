<?php

namespace Deporvillage\OrderService\Service;

use Pheanstalk\Pheanstalk;

class BeanstalkQueueService extends QueueService
{
    protected $host;

    protected $port;

    protected $job;

    /** @var  \Pheanstalk\Pheanstalk $queue */
    protected $queue;

    public function __construct($host, $port, $queueName)
    {
        $this->host = $host;
        $this->port = $port;
        parent::__construct($queueName);
    }

    protected function getQueue()
    {
        if (is_null($this->queue)) {
            $this->queue = new Pheanstalk($this->host, $this->port);
        }

        return $this->queue;

    }

    public function push($jobData)
    {
        $this->getQueue()->useTube($this->getQueueName())->put(json_encode($jobData));

        return true;
    }

    public function pop($timeout = 0)
    {
        if ($job = $this->getQueue()->watchOnly($this->getQueueName())->reserve($timeout)) {
            $data = json_decode($job->getData(), true);
            $this->job = $job;

            return $data;
        }

        return null;
    }

    public function clear()
    {
        if ($this->job) {
            $this->getQueue()->delete($this->job);
            $this->job = null;
        }
    }

    public function bury()
    {
        if ($this->job) {
            $this->getQueue()->bury($this->job);
            $this->job = null;
        }
    }
}
