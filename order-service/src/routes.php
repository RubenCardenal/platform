<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Define app routes
$app->get('/hello/{name}', function ($request, $response, $args) {
    return $response->write("Hello " . $args['name']);
});

$app->get('/', function($request, $response, $args){
   return $response->write("Yeah!"); 
});
