<?php
use Monolog\Logger;
use Deporvillage\OrderService\Service\OrderImportService;
use Deporvillage\OrderService\Repository\OrderRepository;
use Deporvillage\OrderService\Repository\ApiOrderRepository;
use Deporvillage\OrderService\Service\BeanstalkQueueService;
use Deporvillage\OrderService\Command\OrderImportCommand;
use Symfony\Component\Console\Application;

$container = $app->getContainer();

$container['db'] = function ($c) {
    $db = $c['settings']['db'];
    $uri = "mongodb://" . $db['user'] . ':' . $db['password'] . '@' . $db['hostname'] . ':' . $db['port'];
    $client = new MongoDB\Client($uri);
    return $client->selectDatabase($db['dbname']);
};

$container[Logger::class] = function ($c) {
    $logger = new \Monolog\Logger($c['settings']['logger']['name']);
    $formatter = new \Monolog\Formatter\LineFormatter(null, null, false, true);

    $logger->pushProcessor(new \Monolog\Processor\UidProcessor());

    $stdoutHandler = new \Monolog\Handler\StreamHandler(
        'php://stdout',
        $c['settings']['logger']['level']
    );
    $stdoutHandler->setFormatter($formatter);
    $logger->pushHandler($stdoutHandler);

    $logfileHandler = new \Monolog\Handler\StreamHandler(
        $c['settings']['logger']['path'],
        $c['settings']['logger']['level']
    );
    $logfileHandler->setFormatter($formatter);
    $logger->pushHandler($logfileHandler);

    return $logger;
};

/** Repositories **/
$container[OrderRepository::class] = function ($c) {
    return new OrderRepository($c['db']);
};

$container[ApiOrderRepository::class] = function ($c) {
    return new ApiOrderRepository($c['settings']['magento_api']);
};

/** Services */
$container[BeanstalkQueueService::class] = function ($c) {
    return new BeanstalkQueueService(
        $c['settings']['beanstalk']['host'],
        $c['settings']['beanstalk']['port'],
        $c['settings']['beanstalk']['queueName']
    );
};

$container[OrderImportService::class] = function ($c) {
    return new OrderImportService(
        $c[OrderRepository::class],
        $c[ApiOrderRepository::class],
        $c[BeanstalkQueueService::class],
        $c[Logger::class]
    );
};

/** Commands **/
$container[OrderImportCommand::class] = function ($c) {
    return new OrderImportCommand(
        $c[OrderImportService::class],
        $c[Logger::class]
    );
};

$container['console_app'] = function ($c) {
    $application = new Application(
        $c['settings']['app_name'],
        $c['settings']['app_version']
    );
    $application->addCommands(
        [
            $c[OrderImportCommand::class]
        ]
    );

    return $application;
};