<?php

namespace Deporvillage\OrderService\Builder;

use Deporvillage\OrderService\Model\OrderStatusCodes;

class StatusBuilder
{
    public static function buildStatus($statusCode)
    {
        if (!OrderStatusCodes::isValid($statusCode)) {
            return new \Exception("Invalid Status Code provided - " . $statusCode);
        }

        $status = new \stdClass();
        $status->code = $statusCode;
        $status->updated_at = new \DateTime();

        return $status;
    }
}