<?php

namespace Deporvillage\OrderService\Builder;

class OrderBuilder
{
    public static function buildOrder(
        $orderId,
        \stdClass $currentStatus,
        \stdClass $magentoData
    ) {
        $order = new \stdClass();

        $order->order_id = $orderId;
        $order->current_status = $currentStatus;
        $order->statuses[] = $currentStatus;
        $order->magento_data = $magentoData;
        $order->created_at = new \DateTime();

        return $order;
    }
}