<?php

namespace Deporvillage\OrderService\Model;

use MyCLabs\Enum\Enum;

class OrderAction extends Enum
{
    const CREATE = 'order.create';
    const CANCEL = 'order.cancel';
}