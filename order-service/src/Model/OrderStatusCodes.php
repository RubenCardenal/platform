<?php

namespace Deporvillage\OrderService\Model;

use MyCLabs\Enum\Enum;

class OrderStatusCodes extends Enum
{
    /** @var string Order is created in Magento (Pending) */
    const CREATED = "CREATED";
    /** @var FUTURE: Order is Fraud OK and then is sent to Open Bravo, ESBO */
    const ACCEPTED = "ACCEPTED";
    /** @var string Order is Picked, Packed & awaiting Carrier Pickup (Repartos) */
    const SHIPPED = "SHIPPED";
    /** @var string Order is Delivered / Customer has picked up the order */
    const DELIVERED = "DELIVERED";
    /** @var string Order has been cancelled */
    const CANCELLED = "CANCELLED";
    /** @var string Customer has requested a return */
    const RETURNREQUESTED = "RETURNREQUESTED";
    /** @var string Order has been Returned (ESBO has processed the return) */
    const RETURNED = "RETURNED";
    /** @var string Order has been Refunded (Deporvillage has refunded the amount) */
    const REFUNDED = "REFUNDED";
}