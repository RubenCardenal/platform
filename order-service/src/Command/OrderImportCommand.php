<?php

namespace Deporvillage\OrderService\Command;

use Deporvillage\OrderService\Service\OrderImportService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class OrderImportCommand extends Command
{
    /** @var OrderImportService */
    protected $service;

    /** @var LoggerInterface */
    protected $logger;

    protected function configure()
    {
        $this->setName('order:import')->setDescription('Read new order notifications from queue and import orders from Magento API into database');
    }

    public function __construct(
        OrderImportService $service,
        LoggerInterface $logger
    ) {
        $this->service = $service;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->logger->info('Begin command ' . $this->getName());
            $this->service->execute();
            $this->logger->info('End command ' . $this->getName());
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
