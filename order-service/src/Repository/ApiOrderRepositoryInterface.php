<?php

namespace Deporvillage\OrderService\Repository;

interface ApiOrderRepositoryInterface
{
    /**
     * @param string $orderId
     * @return \stdClass|null
     */
    public function get($orderId);
}