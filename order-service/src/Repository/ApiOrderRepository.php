<?php

namespace Deporvillage\OrderService\Repository;

use SoapClient;

class ApiOrderRepository implements ApiOrderRepositoryInterface
{
    /** @var string */
    protected $apiUser;
    /** @var string */
    protected $apiKey;

    /** @var SoapClient */
    protected $soapClient;
    /** @var string */
    protected $sessionId;

    /**
     * @param array $apiConfig
     */
    public function __construct($apiConfig)
    {
        $this->soapClient = new SoapClient($apiConfig['endpoint'], $apiConfig['options']);
        $this->apiUser = $apiConfig['user'];
        $this->apiKey = $apiConfig['key'];
    }

    /**
     * {@inheritdoc}
     */
    public function get($orderId)
    {
        $this->login();

        return $this->soapClient->salesOrderInfo($this->sessionId, $orderId);
    }

    /**
     * @return string
     */
    protected function login()
    {
        if (!isset($this->sessionId)) {
            $this->sessionId = $this->soapClient->login($this->apiUser, $this->apiKey);
        }

        return $this->sessionId;
    }
}