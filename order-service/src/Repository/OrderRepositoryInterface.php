<?php

namespace Deporvillage\OrderService\Repository;

interface OrderRepositoryInterface
{
    /**
     * @param string $id
     * @return array|null|object
     */
    public function findById($id);

    /**
     * @param string $orderId
     * @return array|null
     */
    public function findByOrderId($orderId);

    /**
     * @param array|object $document
     * @return array|object|null
     */
    public function add($document);

    /**
     * @param string $orderId
     * @param \stdClass $status
     * @return array|mixed|null|object
     */
    public function updateStatus($orderId, $status);
}