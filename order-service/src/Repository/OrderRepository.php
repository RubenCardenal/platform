<?php

namespace Deporvillage\OrderService\Repository;

use MongoDB\Database;
use MongoDB\Collection;
use MongoDB\Operation\FindOneAndUpdate;

class OrderRepository implements OrderRepositoryInterface
{
    const COLLECTION_NAME = 'orders';

    /** @var Collection */
    protected $dbCollection;

    /**
     * @param Database $db
     */
    public function __construct(Database $db)
    {
        $this->dbCollection = $db->selectCollection(self::COLLECTION_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function findById($id)
    {
        return $this->dbCollection->findOne(["_id" => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function findByOrderId($orderId)
    {
        $filter['order_id'] = $orderId;
        $options = [
            "projection" => [
                "_id" => false,
                "order_id" => true
            ]
        ];

        return $this->dbCollection->findOne($filter, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function add($document)
    {
        $result = $this->dbCollection->insertOne($document);

        return $this->findById($result->getInsertedId());
    }

    /**
     * {@inheritdoc}
     */
    public function updateStatus($orderId, $status)
    {
        $filter['order_id'] = $orderId;
        $update['$addToSet']['statuses'] = $status;
        $update['$set']['current_status'] = $status;
        $options = ['returnDocument' => FindOneAndUpdate::RETURN_DOCUMENT_AFTER];

        return $this->dbCollection->findOneAndUpdate($filter, $update, $options);
    }
}