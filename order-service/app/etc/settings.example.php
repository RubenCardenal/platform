<?php
return [
    'app_name' => 'Order Service',
    'app_version' => '0.0.1',
    'displayErrorDetails' => true, // set to false in production
    'addContentLengthHeader' => false, // Allow the web server to send the content-length header
    'db' => [
        'hostname' => 'mongo',
        'port' => '27017',
        'dbname' => 'order-service',
        'user' => 'root',
        'password' => 'example'
    ],
    'logger' => [
        'name' => 'order-service',
        'path' => __DIR__ . '/../../logs/app.log',
        'level' => \Monolog\Logger::DEBUG,
    ],
    'beanstalk' => [
        'host' => 'beanstalk-pre.deporvillage.tech',
        'port' => 11300,
        'queueName' => 'order-service'
    ],
    'worker' => [
        'max_runtime' => 3600
    ],
    'magento_api' => [
        'endpoint' => 'https://www.deporvillage.com/api/v2_soap?wsdl',
        'user' => '<API_USER>',
        'key' => '<API_KEY>',
        'options' => [
            'trace' => 1,
            'exceptions' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE
        ]
    ]
];