<?php

ini_set("soap.wsdl_cache_enabled", 0);
ini_set('soap.wsdl_cache_ttl', 1);

set_time_limit(0);
date_default_timezone_set('Europe/Madrid');

require __DIR__ . '/../vendor/autoload.php';

// Configure and Create Slim app
$settings = require __DIR__ . '/etc/settings.php';
$app = new \Slim\App(['settings' => $settings]);

// Register Services
require __DIR__ . '/../src/services.php';
